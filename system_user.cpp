#include <stddef.h>  // defines NULL
#include "system_user.h"


/** Global static pointer used to ensure a
 *  single instance of the class.
**/
SystemUser* SystemUser::m_pInstance = NULL;

/** This function is called to create an instance of the class.
 *  Calling the constructor publicly is not allowed. The
 *  constructor is private and is only called by this Instance function.
 **/

SystemUser* SystemUser::Instance()
 {
    if (!m_pInstance)   // Only allow one instance of class to be generated.
       m_pInstance = new SystemUser;

    return m_pInstance;
 }

UserDetails*        SystemUser::user_details_             = new UserDetails();
RecipientsDetails   SystemUser::current_recipient_details;
StaffDetails        SystemUser::current_staff_details;
SchoolDetails       SystemUser::current_school_search_range;
bool                SystemUser::login_status_             = false;
reportType          SystemUser::current_report_selected;


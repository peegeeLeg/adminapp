#ifndef DATABASE_CONTROLLER
#define DATABASE_CONTROLLER

#include <database.h>

/*!
 * \brief The DatabaseController class
 */
class DatabaseController
{
public:
  DatabaseController();
  ~DatabaseController();

  // db handler
  static Database* local_db_handle_;
  static Database* remote_db_handle_;
  static Database* active_db_handle_;
};

#endif // DATABASE_CONTROLLER


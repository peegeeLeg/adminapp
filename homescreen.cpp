#include "homescreen.h"
#include "ui_homescreen.h"

#include <QDebug>
#include <QDir>
#include <QFile>

/*!
 * \brief HomeScreen::HomeScreen
 * \param parent
 */
HomeScreen::HomeScreen(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::HomeScreen)
{
  // set parent
  ui->setupUi(this);
  // maximize screen
  this->showMaximized();
  // set window title
  this->setWindowTitle(QString("Please login"));

  // init db controller
  db_controller_              = new DatabaseController;


  // setup all application screens
  newUserAdmin                = new UserAdministration;
  newLoginScreen              = new Login_Screen;
  newFingerprintScannerWindow = new FingerprintScanner;
  newIdentificationScreen     = new IdentificationScreen;


  newFingerprintScannerWindow->initializeScanner();

  // initialise temp variables
  login_state = false;

  // set background
   ui->hs_lbl_info_main->setPixmap(QPixmap::fromImage(QImage(":/Company/Logo/kumaka logo.png")));
  // set Icon
  this->setWindowIcon(QIcon(":/Company/Logo/kumaka Icon.png"));

  // - Signals and Slots
  connect(newLoginScreen, SIGNAL(signal_login(bool, userType)), this, SLOT(on_login(bool, userType)));



  // Login USER
  ApplicationLogin();
  //login_state = true;
  // check state
  if(!login_state)
  {
    // disable Screen
    this->setEnabled(false);

    // set window title
    this->setWindowTitle(QString("Failed to login"));

    // display error
    ui->hs_lbl_info_error->setText(errorMsg("Login Failed! - Please restart application"));
  }
  else
  {
    // create home screen for specified user
    switch(SystemUser::Instance()->getUserType())
   // switch(SuperAdmin)
    {
    case SuperAdmin:
      // set window title
      this->setWindowTitle(QString("%1 %2 - (Super Administrator)")
                           .arg(SystemUser::Instance()->getName())
                           .arg(SystemUser::Instance()->getSurname()));
      break;
    case Admin:
      // set window title
      this->setWindowTitle(QString("%1 %2 - (Administrator)")
                           .arg(SystemUser::Instance()->getName())
                           .arg(SystemUser::Instance()->getSurname()));

      break;
    case Nurse:
      // set window title
      this->setWindowTitle(QString("%1 %2 - (Nurse)")
                           .arg(SystemUser::Instance()->getName())
                           .arg(SystemUser::Instance()->getSurname()));
      // set widgets
      ui->menuAdministration->setEnabled(false);
      break;
    case generalUser:
      break;
    }

    // Set Dash board values
    double temp_value  = 0.0;
    double temp_value2 = 0.0;
    QString temp_string;

    // current stock count
    DatabaseController::active_db_handle_->getDignityPackCounts(temp_value);
    DatabaseController::active_db_handle_->getDignityPackLimitDetails(temp_value2,temp_string);

    // update
    QApplication::processEvents();
  }
}


/*!
 * \brief HomeScreen::~HomeScreen
 */
HomeScreen::~HomeScreen()
{
  delete ui;
}

void HomeScreen::ApplicationLogin()
{
  // load login screen
  newLoginScreen->setModal(true);
  newLoginScreen->exec();
}

void HomeScreen::on_login(bool loginState,\
                          userType typeUser)
{
  // set value
  login_state = loginState;
  user_type   = typeUser;

}

// ---------------------------------
// Home Screen -> Menu Items (SLOTS)
// ---------------------------------

// ---------------------------
// Home Screen -> Menu -> File
// ---------------------------
void HomeScreen::on_actionConfiguration_triggered()
{

}

void HomeScreen::on_actionLogout_triggered()
{
  // close app
  this->close();
}

// -------------------------------------------------
// Home Screen -> Menu -> Applications (Nurse)
// -------------------------------------------------



/*!
 * \brief Authenticate_Recipient_triggered
 */
void HomeScreen::on_actionAuthenticate_Recipient_triggered()
{
  //call fingerprint scanner
  on_hs_tb_authenticate_clicked();
}








// -------------------------------------
// Home Screen -> Menu -> Administration
// -------------------------------------
/*!
 * \brief HomeScreen::on_actionNew_triggered
 */
void HomeScreen::on_actionNew_triggered()
{
  // load login screen
  newUserAdmin->setModal(true);

  // set type
  newUserAdmin->setAdminTypeScreen(AddUser);

  // start screen
  newUserAdmin->exec();
}

/*!
 * \brief HomeScreen::on_actionEdit_triggered
 */
void HomeScreen::on_actionEdit_triggered()
{
  // load login screen
  newUserAdmin->setModal(true);

  // set type
  newUserAdmin->setAdminTypeScreen(EditUser);

  // start screen
  newUserAdmin->exec();
}

void HomeScreen::on_actionRemove_User_triggered()
{
  // load login screen
  newUserAdmin->setModal(true);

  // set type
  newUserAdmin->setAdminTypeScreen(DeleteUser);

  // start screen
  newUserAdmin->exec();
}

// ---------------------------
// Home Screen -> Quick Links
// ---------------------------

/*!
 * \brief HomeScreen::on_hs_tb_parent_sync_clicked
 */
//void HomeScreen::on_hs_tb_parent_sync_clicked()
//{

//}

/*!
 * \brief HomeScreen::on_hs_tb_authenticate_clicked
 */
void HomeScreen::on_hs_tb_authenticate_clicked()
{
  // load login screen
  newIdentificationScreen     = new IdentificationScreen;
  newIdentificationScreen->setModal(true);
  newIdentificationScreen->exec();
}














#include "identification_screen.h"
#include "ui_identification_screen.h"
#include <fingerprintscanner.h>
#include <QMessageBox>
#include <QMovie>

IdentificationScreen::IdentificationScreen(QWidget *parent) :
  QDialog(parent, Qt::CustomizeWindowHint),
  ui(new Ui::IdentificationScreen)
{
  // const int variable
  int cZero = 0;

  // set screen and parent
  ui->setupUi(this);

  // set window to be on top of parent
  this->setWindowFlags(Qt::WindowStaysOnTopHint);

  // set window title
  this->setWindowTitle(QString("Identification Screen"));


  // initialise user
  type_user = generalUser;

  // initialise variables
  login_tries = 0;

  // setup Screen
  ui->ls_lbl_icon_info->setPixmap(QPixmap::fromImage(QImage((":/Company/Logo/kumaka logo.png"))));
  ui->ls_pb_scan_finger->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->ls_pb_scan_finger->setIconSize(ui->ls_pb_scan_finger->size());

  /// indicate db access status to ui TODO


  // Change colour of frame to white
  QPalette pal = this->palette();
  pal.setColor(QPalette::Window, Qt::white);
  this->setPalette(pal);

  // add loading scene
  QMovie *movie = new QMovie(":/Company/Logo/loading.gif");
  ui->ls_lbl_info_diplsyProgress->setMovie(movie);
  movie->start();

  identification_finger_scanner = new FingerprintScanner();

  if(identification_finger_scanner->initializeScanner())
  {
    ui->ls_lbl_info_main->setText(successMsg("Press button below..."));

    // change to green tick
    ui->ls_lbl_info_diplsyProgress->setPixmap(QPixmap::fromImage(QImage(":/Company/Logo/greenTick2.jpg")));
  }

  // hide scan results screen
  ui->is_frame_scan_results->setVisible(false);
  ui->is_frame_scan_authorised->setVisible(false);

  // hide issurer button
  ui->is_pb_issuer->setVisible(false);

  // get all districts in current db
  DatabaseController::active_db_handle_->getAllRecipientsRegions(regions_list);

  // set first lists
  for(int i = 0; i < regions_list.size(); i++)
    ui->is_cmBx_districts->addItem(regions_list.at(i).district);

  QList< QPair<QString, int> > temp_str_list;

  if(!regions_list.isEmpty())
    DatabaseController::active_db_handle_->getSchoolsByDistricId(temp_str_list,regions_list.at(cZero).region_id);
  else
    temp_str_list.clear();

  ui->is_cmBx_schools->clear();

  // populate
  for(int i = 0; i < temp_str_list.size(); i++)
  {
    if(i == 0)
      DatabaseController::active_db_handle_->getSchoolDetailsByIdAndRegionId(SystemUser::Instance()->current_school_search_range,
                                                                             temp_str_list.at(i).second,
                                                                             regions_list.at(cZero).region_id);
    ui->is_cmBx_schools->addItem(temp_str_list.at(i).first);
  }


  ui->is_ckBx_all_districs->setChecked(true);
  all_districs_selected = true;

  ui->is_ckBx_all_schools->setChecked(true);
  all_schools_selected = true;

}

IdentificationScreen::~IdentificationScreen()
{
  delete ui;
}


/*!
 * \brief IdentificationScreen::on_ls_pb_scan_finger_clicked
 */
void IdentificationScreen::on_ls_pb_scan_finger_clicked()
{
  QByteArray            probe_template;
  QPair<QByteArray,int> stored_template;
  QList<int>            template_ids_list;
  QList<int>            recipient_ids_list;

  bool                  verify_success;
  int                   matched_id;

  // important to clear this lists
  recipient_ids_list.clear();
  template_ids_list.clear();
  ui->is_lbl_error_info->clear();

  // capture probe finger
  int temp;
  identification_finger_scanner->capture(probe_template,temp);

  // get stored templates by range settings

  verify_success  = false;
  matched_id      = 0;

  // get all ids of the stored template
  if(ui->is_ckBx_all_districs->isChecked()){
    //- get all ids of the stored template
    DatabaseController::active_db_handle_->getAllTemplateIds(template_ids_list,
                                                             nonUser);
  }
  else
  {
    if (ui->is_ckBx_all_schools->isChecked()){
      //- get district id as selected
      int         district_id;

      DatabaseController::active_db_handle_->
          getIdByDistricName(ui->is_cmBx_districts->currentText(),district_id);

      //- get all ids of recipients in this region
      recipient_ids_list.clear();
      DatabaseController::active_db_handle_->getRecipientsIdsByRegion(district_id,
                                                                      recipient_ids_list);
    }
    else{
      //- get school id as selected
      int         school_id;

      //- wont work for duplicate school names
      DatabaseController::active_db_handle_->getIdBySchoolName(ui->is_cmBx_schools->currentText(),school_id);

      //- get all ids of recipients in this region
      recipient_ids_list.clear();
      DatabaseController::active_db_handle_->getRecipientsIdsBySchool(school_id,
                                                                      recipient_ids_list);
    }
  }

  if(recipient_ids_list.isEmpty() && template_ids_list.isEmpty())
  {
    //- get templates ids by region recipients ids
    template_ids_list.clear();

    for(int i = 0; i < recipient_ids_list.size(); i++)
      DatabaseController::active_db_handle_->getTemplateIdsByRecipientId(recipient_ids_list.at(i),
                                                                         template_ids_list);
  }


  if(template_ids_list.isEmpty())
  {
    ui->is_lbl_error_info->setText(warningMsg("No recipients fingerprints stored!"));
    ui->is_lbl_results_msg->setText(warningMsg("No recipients fingerprints stored!"));
  }

  // assess against stored
  for(int i = 0; i < template_ids_list.size(); i++)
  {
    //- clear container
    stored_template.first.clear();

    //- get next stored template
    DatabaseController::active_db_handle_->getTemplateById(template_ids_list.at(i),
                                                           stored_template,
                                                           nonUser);

    // verify
    if(identification_finger_scanner->verify(probe_template,stored_template.first))
    {
      verify_success  = true;
      matched_id      = stored_template.second;
      break;
    }
  }

  // update results
  if(verify_success)
  {
    QString     temp_str;

    // get the recipient's details
    DatabaseController::active_db_handle_->getRecipientById(matched_id,
                                                            SystemUser::current_recipient_details);

    if(SystemUser::current_recipient_details.recipient_type == Recipient)
    {
      // set receiver type
      temp_reciever = SELF;

      // update results screen
      ui->is_lbl_scanned_rec_names->setText(SystemUser::current_recipient_details.name +
                                            " "                                         +
                                            SystemUser::current_recipient_details.surname);

      DatabaseController::active_db_handle_->getDistricNameById(temp_str,
                                                                SystemUser::current_recipient_details.region_id);

      ui->is_lbl_scanned_rec_district->setText(temp_str);

      DatabaseController::active_db_handle_->getSchoolNameById(temp_str,
                                                               SystemUser::current_recipient_details.school_id);

      ui->is_lbl_scanned_rec_school->setText(temp_str);

      ui->is_lbl_results_error->setText(successMsg("Match found!"));

      ui->is_pb_confim->setEnabled(true);

      ui->is_frame_identification->setVisible(false);
      ui->is_frame_scan_results->setVisible(true);
      ui->is_frame_scan_authorised->setVisible(false);
    }
    else if(SystemUser::current_recipient_details.recipient_type == Authorised)
    {
      // set receiver type
      temp_reciever = AUTHORIZED;

      // set auth names
      ui->is_lbl_scanned_auth_names_label->setText("Authorised Names :");
      ui->is_lbl_scanned_auth_names->setText(SystemUser::current_recipient_details.name +
                                             " "                                         +
                                             SystemUser::current_recipient_details.surname);

      DatabaseController::active_db_handle_->getDistricNameById(temp_str,
                                                                SystemUser::current_recipient_details.region_id);
      ui->is_lbl_scanned_auth_district->setText(temp_str);

      temp_str = "";

      DatabaseController::active_db_handle_->getSchoolNameById(temp_str,
                                                               SystemUser::current_recipient_details.school_id);

      ui->is_lbl_scanned_auth_school->setText(temp_str);

      ui->is_lbl_auth_msg->setText(successMsg("Authorised Match found!"));

      // enable done button
      ui->is_pb_confim_2->setEnabled(true);

      // disable received btn
      ui->is_pb_auth_received->setEnabled(false);

      // disable received stats
      setVisibleAuthReceivedStats(false);

      // reset/populate recipeints list on auth screen
      populateRecipientsOnAuthScreen();

      // enable results screen
      ui->is_frame_identification->setVisible(false);
      ui->is_frame_scan_results->setVisible(false);
      ui->is_frame_scan_authorised->setVisible(true);
    }
  }
  else
  {
    ui->is_pb_confim->setEnabled(false);
    ui->is_pb_confim_2->setEnabled(false);
    ui->is_lbl_results_error->setText(errorMsg("No Match Found!"));
    // enable results screen
    ui->is_frame_identification->setVisible(false);
    ui->is_frame_scan_authorised->setVisible(false);
    ui->is_frame_scan_results->setVisible(true);
  }

}


void IdentificationScreen::populateRecipientsOnAuthScreen()
{
  // variables
  QString     temp_str;
  QDate       temp_date;
  QList<int>  recipient_ids_list;

  // get all recipients
  recipients_list.clear();
  recipient_ids_list.clear();

  if(temp_reciever == AUTHORIZED)
    DatabaseController::active_db_handle_->getRecipientsIdsBySchool(SystemUser::current_recipient_details.school_id,recipient_ids_list);
  else if(temp_reciever == ISSUER)
    DatabaseController::active_db_handle_->getRecipientsIdsBySchool(SystemUser::Instance()->current_school_search_range.id,recipient_ids_list);


  // populate recipients
  for(int i = 0; i < recipient_ids_list.size(); i++){
    temp_recipient.clear();
    DatabaseController::active_db_handle_->getRecipientById(recipient_ids_list.at(i), temp_recipient);
    recipients_list.append(temp_recipient);
  }

  for(int i = 0; i < recipients_list.size(); i++)
  {
    temp_date = QDate();

    DatabaseController::active_db_handle_->getLastRecieptDateById(recipients_list.at(i).id,temp_date);

    ui->is_lv_auth_recipients_list->addItem( ( temp_date.isNull() ? QString("None") :
                                                                    ((temp_date == QDate::currentDate()) ? QString("Today"):
                                                                                                           QString(temp_date.toString("yyyy-MM-dd"))))+ "\t| " +
                                             recipients_list.at(i).name + " " + recipients_list.at(i).surname);

    temp_str = "";
  }
}

/*!
 * \brief IdentificationScreen::on_ls_pb_cancel_clicked
 */
void IdentificationScreen::on_ls_pb_cancel_clicked()
{
  if(identification_finger_scanner->isCapturing())
    identification_finger_scanner->abortCapturing();

  // clear screen
  this->clearScreen();
  // close without saving
  this->close();
}

/*!
 * \brief IdentificationScreen::clearScreen
 */
void IdentificationScreen::clearScreen()
{
  ui->is_frame_scan_results->setVisible(false);
  ui->is_frame_identification->setVisible(true);

  ui->is_cmBx_districts->clear();
  ui->is_cmBx_schools->clear();
  regions_list.clear();

  setVisibleAuthReceivedStats(false);
}

void IdentificationScreen::setVisibleAuthReceivedStats(bool visibility)
{
  ui->is_lbl_auth_no_pack_not_collected->setVisible(visibility);
  ui->is_lbl_no_pack_not_collected_label_2->setVisible(visibility);
  ui->is_spnBx_auth_receiving_count->setVisible(visibility);
  ui->is_lbl_receiving_cnt_label_2->setVisible(visibility);
  ui->is_pb_auth_received->setVisible(visibility);
  ui->is_pb_auth_received->setEnabled(visibility);
  ui->is_lbl_auth_receiving_cnt_label->setVisible(visibility);
  if(!visibility)
    ui->is_lv_auth_recipients_list->clear();
}

/*!
 * \brief IdentificationScreen::on_is_cmBx_districts_currentIndexChanged
 * \param index
 */

void IdentificationScreen::on_is_cmBx_districts_currentIndexChanged(int index)
{
  QList< QPair<QString, int> > temp_str_list;

  DatabaseController::active_db_handle_->getSchoolsByDistricId(temp_str_list,index+1);
  ui->is_cmBx_schools->setCurrentText("");

  ui->is_cmBx_schools->clear();

  // populate
  for(int i = 0; i < temp_str_list.size(); i++)
    ui->is_cmBx_schools->addItem(temp_str_list.at(i).first);
}

QString       IdentificationScreen::selected_search_school;
RegionDetails IdentificationScreen::selected_search_region;

void IdentificationScreen::on_is_pb_set_id_range_clicked()
{
  if(ui->is_ckBx_all_districs->isChecked())
    all_districs_selected   = true;
  else
    all_districs_selected   = false;

  if(ui->is_ckBx_all_schools->isChecked())
    all_schools_selected    = true;
  else
    all_schools_selected    = false;

  if(!all_districs_selected)
    selected_search_region  = regions_list.at(ui->is_cmBx_districts->currentIndex());
  if(!all_schools_selected)
    selected_search_school  = ui->is_cmBx_schools->currentText();

  ui->is_lbl_error_info->setText(successMsg("Search range set!"));
}

void IdentificationScreen::on_is_pb_rescan_clicked()
{
  ui->is_lbl_results_error->clear();
  ui->is_lbl_receiving_cnt_label->clear();
  ui->is_lbl_scanned_rec_district->clear();
  ui->is_lbl_scanned_rec_names->clear();
  ui->is_lbl_scanned_rec_school->clear();

  //switch screens
  ui->is_frame_scan_results->setVisible(false);
  ui->is_frame_scan_authorised->setVisible(false);
  ui->is_frame_identification->setVisible(true);
}

void IdentificationScreen::on_is_pb_rescan_2_clicked()
{
  // clear authorised results screen
  ui->is_lbl_scanned_auth_names->clear();
  ui->is_lbl_scanned_auth_district->clear();
  ui->is_lbl_scanned_auth_school->clear();

  ui->is_lbl_scanned_recipient_name->setText("Not Selected");
  ui->is_lbl_scanned_recipient_surname->setText("Not Selected");

  ui->is_lbl_auth_msg->clear();
  ui->is_pb_confim_2->setEnabled(false);
  ui->is_pb_auth_received->setEnabled(false);
  setVisibleAuthReceivedStats(false);

  recipients_list.clear();
  ui->is_lv_auth_recipients_list->clear();
  on_is_pb_rescan_clicked();
}

void IdentificationScreen::on_is_pb_confim_clicked()
{
  // log action
  DatabaseController::active_db_handle_->trail(QString("Authenticated and gave %1 %2 - %3 packages")
                                               .arg(SystemUser::current_recipient_details.name)
                                               .arg(SystemUser::current_recipient_details.surname)
                                               .arg(ui->is_spnBx_receiving_count->value()));

  // update stock
  DatabaseController::active_db_handle_->updateDignityPacks(ui->is_spnBx_receiving_count->value(),
                                                            QString("Distribution"),
                                                            REMOVE);
  // log distribution activity
  DatabaseController::active_db_handle_->log_distribution_activity(ui->is_spnBx_receiving_count->value(),
                                                                   temp_reciever,
                                                                   SystemUser::current_recipient_details.id);

  // updata msg
  ui->is_lbl_error_info->setText(successMsg("Confirmation successful!"));
  // send to scan screen
  this->on_is_pb_rescan_clicked();

}

void IdentificationScreen::on_is_pb_confim_2_clicked()
{
  if(temp_reciever == AUTHORIZED)
  {
  // log action
  DatabaseController::active_db_handle_->trail(QString("Authenticated Authorised %1 %2, who recieved packs for recipeint(s)")
                                               .arg(SystemUser::current_recipient_details.name)
                                               .arg(SystemUser::current_recipient_details.surname));

  // update msg
  ui->is_lbl_error_info->setText(successMsg("Authorised receiving successful!"));
  }
  else if(temp_reciever == ISSUER)
  {
    // log action
    DatabaseController::active_db_handle_->trail(QString(" received packs for recipeint(s)"));

    // update msg
    ui->is_lbl_error_info->setText(successMsg("Issuer receiving successful!"));
  }
  // send to scan screen
  this->on_is_pb_rescan_clicked();
}

void IdentificationScreen::on_is_pb_cancel_clicked()
{
  this->clearScreen();
  this->close();
}

void IdentificationScreen::on_is_pb_cancel_2_clicked()
{
  on_is_pb_cancel_clicked();
}


void IdentificationScreen::on_is_pb_auth_received_clicked()
{
  // update stock
  DatabaseController::active_db_handle_->updateDignityPacks(ui->is_spnBx_auth_receiving_count->value(),
                                                            QString("Distribution"),
                                                            REMOVE);
  // log distribution
  int result = DatabaseController::active_db_handle_->log_distribution_activity(ui->is_spnBx_auth_receiving_count->value(),
                                                                                temp_reciever,
                                                                                temp_recipient.id);

  // disable receipt
  setVisibleAuthReceivedStats(false);
  ui->is_lbl_scanned_recipient_name->setText("Not Selected");
  ui->is_lbl_scanned_recipient_surname->setText("Not Selected");
  ui->is_lbl_auth_no_pack_not_collected->setText("0");

  // reset leaner list
  populateRecipientsOnAuthScreen();

  // message
  if(result == 0)
  {
    ui->is_lbl_auth_msg->setText(successMsg(QString("Packs recieved for/by %1 %2!")
                                            .arg(temp_recipient.name)
                                            .arg(temp_recipient.surname)));
  }
  else if(result == -1)
  {
    ui->is_lbl_auth_msg->setText(successMsg(QString("An error (db conn) occured - Packs "
                                                    "NOT recieved for/by %1 %2!")
                                            .arg(result)));
  }
  else if(result == -2)
  {
    ui->is_lbl_auth_msg->setText(successMsg(QString("An error (query) occured - Packs "
                                                    "NOT recieved for/by %1 %2!")
                                            .arg(result)));
  }
  else
  {
    ui->is_lbl_auth_msg->setText(successMsg(QString("An error (unknown) occured - Packs "
                                                    "NOT recieved for/by %1 %2!")
                                            .arg(result)));
  }

}

void IdentificationScreen::on_is_lv_auth_recipients_list_clicked(const QModelIndex &index)
{
  int temp_int    = 0;
  int ind         = index.row();

  temp_recipient  = recipients_list.at(ind);

  // enable receipt
  setVisibleAuthReceivedStats(true);
  ui->is_lbl_scanned_recipient_name->setText(temp_recipient.name);
  ui->is_lbl_scanned_recipient_surname->setText(temp_recipient.surname);
  DatabaseController::active_db_handle_->getNotCollectedPacksCount(temp_recipient.id,
                                                                   temp_int);
  ui->is_lbl_auth_no_pack_not_collected->setText(QString::number(temp_int));

  // clear msg
  ui->is_lbl_auth_msg->setText(successMsg("Recipient selected!"));
}

void IdentificationScreen::on_is_pb_issuer_clicked()
{
  // check that specific district and school re selected

  if(ui->is_ckBx_all_districs->isChecked() || ui->is_ckBx_all_schools->isChecked())
  {
    ui->is_lbl_error_info->setText(errorMsg("Please select specific District & School"));
    return;
  }

  // set receiver type
  temp_reciever = ISSUER;

  // set auth names
  ui->is_lbl_scanned_auth_names_label->setText("Issuer Names :");
  ui->is_lbl_scanned_auth_names->setText(SystemUser::Instance()->getName() + " " +
                                         SystemUser::Instance()->getSurname());

  ui->is_lbl_scanned_auth_district->setText(ui->is_cmBx_districts->currentText());

  ui->is_lbl_scanned_auth_school->setText(ui->is_cmBx_schools->currentText());

  ui->is_lbl_auth_msg->setText(successMsg("Issuer Enabled to Recieve on behalf!"));

  // enable done button
  ui->is_pb_confim_2->setEnabled(true);

  // disable received btn
  ui->is_pb_auth_received->setEnabled(false);

  // disable received stats
  setVisibleAuthReceivedStats(false);

  // reset/populate recipeints list on auth screen
  populateRecipientsOnAuthScreen();

  // enable results screen
  ui->is_frame_identification->setVisible(false);
  ui->is_frame_scan_results->setVisible(false);
  ui->is_frame_scan_authorised->setVisible(true);

}


void IdentificationScreen::on_is_cmBx_schools_currentTextChanged(const QString&)
{
  // each time the school is changed, update the current school search
  SystemUser::Instance()->current_school_search_range.clear();

  int temp_school_id;
  int temp_district_id;

  DatabaseController::active_db_handle_->getIdBySchoolName(ui->is_cmBx_schools->currentText(),temp_school_id);

  DatabaseController::active_db_handle_->getIdByDistricName(ui->is_cmBx_districts->currentText(),temp_district_id);

  DatabaseController::active_db_handle_->getSchoolDetailsByIdAndRegionId(SystemUser::Instance()->current_school_search_range,
                                                                         temp_school_id,
                                                                         temp_district_id);

  // set all tarcking variables
  on_is_pb_set_id_range_clicked();
}

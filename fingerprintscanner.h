#ifndef FINGERPRINTSCANNER_H
#define FINGERPRINTSCANNER_H

#include <QDialog>
#include <QByteArray>

#include <SystemShare.h>
#include <biomini/UFScanner.h>
#include <biomini/UFDatabase.h>
#include <biomini/UFMatcher.h>
#include <biomini/UFExtractor.h>

#define TEMPLATE_SIZE 384
#define MAX_TEMPLATE_SIZE 384
#define MIN_FP_QUALITY 40
#define SECURITY_LEVEL 3
#define SENSITIVITY_LEVEL 3


namespace Ui {
class FingerprintScanner;
}

class FingerprintScanner : public QDialog
{
  Q_OBJECT

public:
  explicit FingerprintScanner(QWidget *parent = 0);
  ~FingerprintScanner();

  // actual scanner methods

  bool initializeScanner();
  bool capture(QByteArray& fng_template, int &quality);

  bool identify(QList<QByteArray> templates,
                QByteArray fng_template,
                int& index);

  bool verify(QByteArray probe_template,
              QByteArray stored_templae);

  bool abortCapturing();

  bool isCapturing();



private slots:
  void on_fs_pb_scan_finger_clicked();

  void on_fs_pb_confirm_clicked();

  void on_fs_pb_cancel_clicked();

private:
  Ui::FingerprintScanner *ui;

  // actual scanner stuff
  static UFS_STATUS     ufs_res;
  static HUFScanner     hScanner;
  static HUFMatcher     hMatcher;
  static int            TemplateSize;
  static int            nEnrollQuality;
  static unsigned char* Template;

};

#endif // FINGERPRINTSCANNER_H

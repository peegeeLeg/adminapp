/////////////////////////////////////////////////////////////////////////////
//
// UniFinger Engine SDK 3.3
//
// UFExtractor.h
// Header file for UFExtractor module
//
// Copyright (C) 2011 Suprema Inc.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _UFEXTRACTOR_H_
#define _UFEXTRACTOR_H_

#ifdef UFE_EXPORTS
#define UFE_API __declspec(dllexport) __stdcall
#else
#define UFE_API __stdcall
#endif

#ifdef __cplusplus
extern "C" {
#endif


// Status Definition
#define UFE_STATUS						int

// Status Return Values
#define UFE_OK							0
#define UFE_ERROR						-1
#define UFE_ERR_NO_LICENSE				-101
#define UFE_ERR_LICENSE_NOT_MATCH		-102
#define UFE_ERR_LICENSE_EXPIRED			-103
#define UFE_ERR_NOT_SUPPORTED			-111
#define UFE_ERR_INVALID_PARAMETERS		-112
// For Extraction
#define UFE_ERR_NOT_GOOD_IMAGE			-301
#define UFE_ERR_EXTRACTION_FAILED		-302
#define UFE_ERR_UNDEFINED_MODE			-311
// For Extraction: Core Detection
#define UFE_ERR_CORE_NOT_DETECTED		-351
#define UFE_ERR_CORE_TO_LEFT			-352
#define UFE_ERR_CORE_TO_LEFT_TOP		-353
#define UFE_ERR_CORE_TO_TOP				-354
#define UFE_ERR_CORE_TO_RIGHT_TOP		-355
#define UFE_ERR_CORE_TO_RIGHT			-356
#define UFE_ERR_CORE_TO_RIGHT_BOTTOM	-357
#define UFE_ERR_CORE_TO_BOTTOM			-358
#define UFE_ERR_CORE_TO_LEFT_BOTTOM		-359

// Parameters
#define UFE_PARAM_DETECT_CORE			301
#define UFE_PARAM_TEMPLATE_SIZE			302
#define UFE_PARAM_USE_SIF				311

// Modes (Scanner Type)
#define UFE_MODE_SFR200					1001
#define UFE_MODE_SFR300					1002
#define UFE_MODE_SFR300v2				1003
#define UFE_MODE_SFR300v2_Ver2			1004
#define UFE_MODE_SFR500					1005
#define UFE_MODE_GENERAL				1006

// Template Type
#define UFE_TEMPLATE_TYPE_SUPREMA		2001
#define UFE_TEMPLATE_TYPE_ISO19794_2	2002
#define UFE_TEMPLATE_TYPE_ANSI378		2003

#define UFE_MAX_IMAGE_BUFFER_LENGTH		100000


typedef void* HUFExtractor;


UFE_STATUS UFE_API UFE_Create(HUFExtractor* phExtractor);
UFE_STATUS UFE_API UFE_Delete(HUFExtractor hExtractor);

UFE_STATUS UFE_API UFE_GetMode(HUFExtractor hExtractor, int* pnMode);
UFE_STATUS UFE_API UFE_SetMode(HUFExtractor hExtractor, int nMode);

UFE_STATUS UFE_API UFE_GetParameter(HUFExtractor hExtractor, int nParam, void* pValue);
UFE_STATUS UFE_API UFE_SetParameter(HUFExtractor hExtractor, int nParam, void* pValue);

UFE_STATUS UFE_API UFE_Extract(HUFExtractor hExtractor, unsigned char* pImage, int nWidth, int nHeight, int nResolution, unsigned char* pTemplate, int* pnTemplateSize, int* pnEnrollQuality);

UFE_STATUS UFE_API UFE_SetEncryptionKey(HUFExtractor hExtractor, unsigned char* pKey);
UFE_STATUS UFE_API UFE_EncryptTemplate(HUFExtractor hExtractor, unsigned char* pTemplateInput, int nTemplateSize, unsigned char* pTemplateOutput, int* pnTemplateOutputSize);
UFE_STATUS UFE_API UFE_DecryptTemplate(HUFExtractor hExtractor, unsigned char* pTemplateInput, int nTemplateSize, unsigned char* pTemplateOutput, int* pnTemplateOutputSize);

UFE_STATUS UFE_API UFE_LoadImageFromBMPFile(const char* szBMPFileName, unsigned char* pImage, int* pnWidth, int* pnHeight);
UFE_STATUS UFE_API UFE_LoadImageFromBMPBuffer(unsigned char* pBMPBuffer, int nBMPBufferSize, unsigned char* pImage, int* pnWidth, int* pnHeight);

UFE_STATUS UFE_API UFE_GetFeatureNumber(HUFExtractor hExtractor, unsigned char* pTemplate, int nTemplateSize, int* pnFeatureNumber);

UFE_STATUS UFE_API UFE_GetErrorString(UFE_STATUS res, char* szErrorString);

UFE_STATUS UFE_API UFE_SetTemplateType(HUFExtractor hExtractor, int nTemplateType);
UFE_STATUS UFE_API UFE_GetTemplateType(HUFExtractor hExtractor, int *nTemplateType);

UFE_STATUS UFE_API UFE_DrawImage(unsigned char* pImage, int nWidth, int nHeight, HDC hDC, int nLeft, int nTop, int nRight, int nBottom);

UFE_STATUS UFE_API UFE_LoadImageFromWSQFile(const char* szWSQFileName, unsigned char* pImage, int* pnWidth, int* pnHeight);
UFE_STATUS UFE_API UFE_LoadImageFromWSQBuffer(unsigned char* pWSQBuffer, int nWSQBufferSize, unsigned char* pImage, int* pnWidth, int* pnHeight);

UFE_STATUS UFE_API UFE_GetImageBufferTo19794_4ImageBuffer(HUFExtractor hExtractor, unsigned char* pImageDataIn, int Width, int Height, unsigned char* pImageDataOut, int* pImageDataLength);
UFE_STATUS UFE_API UFE_GetImageBufferToBMPImageBuffer(HUFExtractor hExtractor, unsigned char* pImageDataIn, int Width, int Height, unsigned char* pImageDataOut, int* pImageDataLength);

// Blew two functions are provided on x86 only
UFE_STATUS UFE_API UFE_GetImageBufferToJPEGImageBuffer(HUFExtractor hExtractor, unsigned char* pImageDataIn, int Width, int Height, unsigned char* pImageDataOut, int* pImageDataLength);
UFE_STATUS UFE_API UFE_GetImageBufferToJP2ImageBuffer(HUFExtractor hExtractor, unsigned char* pImageDataIn, int Width, int Height, unsigned char* pImageDataOut, int* pImageDataLength);

#ifdef __cplusplus
}
#endif

#endif // _UFEXTRACTOR_H_

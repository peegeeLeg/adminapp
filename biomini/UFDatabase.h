/////////////////////////////////////////////////////////////////////////////
//
// UniFinger Engine SDK 3.1
//
// UFDatabase.h
// Header file for UFDatabase module
//
// Copyright (C) 2007 Suprema Inc.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef _UFDATABASE_H_
#define _UFDATABASE_H_

#ifdef UFD_EXPORTS
#define UFD_API __declspec(dllexport) __stdcall
#else
#define UFD_API __stdcall
#endif

#ifdef __cplusplus
extern "C" {
#endif


// Status Definition
#define UFD_STATUS						int

// Status Return Values
#define UFD_OK							0
#define UFD_ERROR						-1
#define UFD_ERR_NO_LICENSE				-101
#define UFD_ERR_LICENSE_NOT_MATCH		-102
#define UFD_ERR_LICENSE_EXPIRED			-103
#define UFD_ERR_NOT_SUPPORTED			-111
#define UFD_ERR_INVALID_PARAMETERS		-112
// For Database
#define UFD_ERR_SAME_FINGER_EXIST		-501


typedef void* HUFDatabase;


UFD_STATUS UFD_API UFD_Open(const char* szConnection, const char* szUserID, const char* szPassword, HUFDatabase* phDatabase);
UFD_STATUS UFD_API UFD_Close(HUFDatabase hDatabase);

UFD_STATUS UFD_API UFD_AddData(HUFDatabase hDatabase, const char* szUserID, int nFingerIndex,
	unsigned char* pTemplate1, int nTemplate1Size, unsigned char* pTemplate2, int nTemplate2Size, const char* szMemo);

UFD_STATUS UFD_API UFD_UpdateDataByUserInfo(HUFDatabase hDatabase, const char* szUserID, int nFingerIndex,
	unsigned char* pTemplate1, int nTemplate1Size, unsigned char* pTemplate2, int nTemplate2Size, const char* szMemo);
UFD_STATUS UFD_API UFD_UpdateDataBySerial(HUFDatabase hDatabase, int nSerial,
	unsigned char* pTemplate1, int nTemplate1Size, unsigned char* pTemplate2, int nTemplate2Size, const char* szMemo);

UFD_STATUS UFD_API UFD_RemoveDataByUserID(HUFDatabase hDatabase, const char* szUserID);
UFD_STATUS UFD_API UFD_RemoveDataByUserInfo(HUFDatabase hDatabase, const char* szUserID, int nFingerIndex);
UFD_STATUS UFD_API UFD_RemoveDataBySerial(HUFDatabase hDatabase, int nSerial);
UFD_STATUS UFD_API UFD_RemoveAllData(HUFDatabase hDatabase);

UFD_STATUS UFD_API UFD_GetDataNumber(HUFDatabase hDatabase, int* pnDataNumber); 
UFD_STATUS UFD_API UFD_GetDataByIndex(HUFDatabase hDatabase, int nIndex,
	int* pnSerial, char* szUserID, int* pnFingerIndex, unsigned char* pTemplate1, int* pnTemplate1Size, unsigned char* pTemplate2, int* pnTemplate2Size, char* szMemo);
UFD_STATUS UFD_API UFD_GetDataByUserInfo(HUFDatabase hDatabase, const char* szUserID, int nFingerIndex,
	unsigned char* pTemplate1, int* pnTemplate1Size, unsigned char* pTemplate2, int* pnTemplate2Size, char* szMemo);
UFD_STATUS UFD_API UFD_GetDataBySerial(HUFDatabase hDatabase, int nSerial,
	char* szUserID, int* pnFingerIndex, unsigned char* pTemplate1, int* pnTemplate1Size, unsigned char* pTemplate2, int* pnTemplate2Size, char* szMemo);

UFD_STATUS UFD_API UFD_GetTemplateNumber(HUFDatabase hDatabase, int* pnTemplateNumber);
UFD_STATUS UFD_API UFD_GetTemplateListWithSerial(HUFDatabase hDatabase, unsigned char** ppTemplate, int* pnTemplateSize, int* pnSerial);

UFD_STATUS UFD_API UFD_GetErrorString(UFD_STATUS res, char* szErrorString);


#ifdef __cplusplus
}
#endif

#endif // _UFDATABASE_H_

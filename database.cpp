#include <QDebug>
#include "database.h"


/*!
 * \brief Database::Database
 * \param db_system
 * \param db_host
 * \param db_name
 * \param db_username
 * \param db_password
 */
Database::Database(dbSystem       dbSystemType,
                   QString        dbHost,
                   QString        dbName,
                   QString        dbUsername,
                   QString        dbPassword)
{

    db_system_       = dbSystemType;
    db_host_         = dbHost;
    db_name_         = dbName;
    db_username_     = dbUsername;
    db_password_     = dbPassword;
    db_conn_state_   = false;

}


/*!
 * \brief Database::~Database
 */
Database::~Database()
{
    db_handler_ = QSqlDatabase::database();
    db_handler_.close();
}

/*!
 * \brief Database::initialiseDb
 * \return -1 if failed to initialize the corresponing db
 *         0 if successfully initialized database
 */
int Database::initialiseDb()
{
    // check if db handle is active now
    if(db_handler_.isOpen() && db_conn_state_ == true)
    {
        qDebug()<<"Database::initialiseDb: "<< db_name_
               <<"is already running...";
        return 0;
    }

    //int const variables
    const int kZero     = 0;
    const int kMinusOne = -1;

    db_handler_ = QSqlDatabase::addDatabase(dbSystemString(db_system_));
    db_handler_.setDatabaseName(db_name_);
    db_handler_.setHostName(db_host_);
    db_handler_.setUserName(db_username_);
    db_handler_.setPassword(db_password_);


    if(db_handler_.open())
    {
        qDebug()<<"Database::initialiseDb: "<< db_name_
               <<" - db_handler_.open() Success";
        db_conn_state_ = true;
        return kZero;
    }
    else
    {
        qDebug()<<"Database::initialiseRemoteDb: "<< db_name_
               <<" - remote_db_handler_.open() failed";

        db_conn_state_ = false;
        return kMinusOne;
    }
}

/*!
 * \brief Database::verifyCredentials
 * \param userName
 * \param passWord
 * \param typeOfUser
 * \param name
 * \param surname
 * \param email
 * \param contact_number
 * \param statusOfAcc
 * \return 0 match found
 *         -1 db conn false
 *         -2 username or password not entered
 *         -3 wrong Username/Password
 *         -4 user account blocked
 *         -5 query error
 */
int Database::verifyCredentials(QString     userName,
                                QString     passWord,
                                userType&   typeOfUser,
                                int&        id,
                                QString&    name,
                                QString&    surname,
                                QString&    email,
                                QString&    contact_number,
                                accStatus&  statusOfAcc)
{
    // const int variables
    const int cZero       = 0;  // match found
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // username or password not entered
    const int cMinusThree = -3; // wrong Username/Password
    const int cMinusFour  = -4; // user account blocked
    const int cMinusFive  = -5; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::VerifyCredentials: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM users where username = (:un)");
    query->bindValue(":un"   , userName);

    if(query->exec())
    {
        //- check if username found
        if(userName.trimmed() == "" || passWord.trimmed() == "")
        {
            qDebug()<< "Database::VerifyCredentials: username or password not entered";
            return cMinusTwo;
        }


        while (query->next())
        {
//            qDebug()<< "---therrer...."<<query->value(query->record().indexOf("password")).toString()<<"endL<<<";
           // QString pass = query->value(query->record().indexOf("password")).toString();

            //-- check if password matches
            if(query->value(query->record().indexOf("password")).toString()       ==
                    hashSha256(passWord))
            {
                //--- check account status
                statusOfAcc = (accStatus)query->value(query->record().indexOf("status")).toInt();

                //--- assign all needed variables if active
                if(statusOfAcc == Active)
                {
                    id              = query->value(query->record().indexOf("id")).toInt();
                    name            = query->value(query->record().indexOf("name")).toString();
                    surname         = query->value(query->record().indexOf("surname")).toString();
                    email           = query->value(query->record().indexOf("email_address")).toString();
                    contact_number  = query->value(query->record().indexOf("contact_number")).toString();
                    typeOfUser      = (userType)query->value(query->record().indexOf("user_type")).toInt();
                    qDebug()<< "Database::VerifyCredentials: statusOfAcc - Active";

                    return cZero;
                }
                else
                {
                    qDebug()<< "Database::VerifyCredentials: statusOfAcc - Blocked";
                    return cMinusFour;
                }
            }
        }
        qDebug()<< "Database::VerifyCredentials: no password/username match";
        return cMinusThree;
    }
    else
    {
        qDebug()<< "Database::VerifyCredentials: query error - "
                << query->lastError()<<" - username  = "<< userName;

        return cMinusFive;
    }
}

/*!
 * \brief Database::addSystemUser
 * \param userName
 * \param passWord
 * \param detailsOfUser*
 * \return 0 system user added successfully
 *         -1 db conn false
 *         -2 username or password not entered
 *         -3 user's Name or Surname not entered
 *         -4 query error
 */
int Database::addSystemUser(QString      userName,
                            QString      passWord,
                            UserDetails* detailsOfUser)
{
    // const int variables
    const int cZero       = 0;  // recipient added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // Name/Surname/username/password not entered
    const int cMinusThree = -3; // fingerprint not enrolled
    const int cMinusFour  = -4; // query error -  details
    const int cMinusFive  = -5; // query error -  fingerprint

    //int variable
    int temp_id = 0;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addSystemUser: db_conn_state false";
        return cMinusOne;
    }

    // check minimus user details filled
    if(detailsOfUser->name.trimmed()    == "" ||
            detailsOfUser->surname.trimmed() == "" ||
            userName.trimmed()               == "" ||
            passWord.trimmed() == ""){
        qDebug()<< "Database::addSystemUser: user's Name/Surname/username/pasword not entered";
        return cMinusTwo;
    }

    // check fingerprint enrolled
    if(detailsOfUser->fingerprint_template == NULL){
        qDebug()<< "Database::addSystemUser: recipient's fingerprint not enrolled";
        return cMinusThree;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO users (name, surname, user_type, username, password,"
                   " status, email_address, contact_number,fingerprint_enrolled ) "
                   "VALUES "
                   "(:name, :surname, :user_type, :username, :password, :status, "
                   ":email_address, :contact_number,:fingerprint_enrolled)");

    query->bindValue(":name"                  , detailsOfUser->name);
    query->bindValue(":surname"               , detailsOfUser->surname);
    query->bindValue(":user_type"             , (int)detailsOfUser->user_type);
    query->bindValue(":username"              , userName);
    query->bindValue(":password"              , hashSha256(passWord));
    query->bindValue(":status"                , (int)detailsOfUser->account_status);
    query->bindValue(":email_address"         , detailsOfUser->email_address);
    query->bindValue(":contact_number"        , detailsOfUser->contact_number);
    query->bindValue(":fingerprint_enrolled"  , detailsOfUser->fingerprint_enrolled);



    if(query->exec())
    {
        qDebug()<< "Database::addSystemUser: successful - ";
    }
    else
    {
        qDebug()<< "Database::addSystemUser: query error details - "
                << query->lastError();

        return cMinusFour;
    }

    query->prepare("SELECT id FROM users WHERE username = (:username) and password = (:password)");

    query->bindValue(":username"  , userName);
    query->bindValue(":password"  , hashSha256(passWord));


    if(query->exec())
    {
        while (query->next())
        {
            temp_id = query->value(query->record().indexOf("id")).toInt();
        }
    }
    else
    {
        qDebug()<< "Database::addSystemUser fingerprint: getting just inserted id query error - "
                << query->lastError();

        return cMinusFive;
    }

    query->prepare("INSERT INTO users_templates ( user_id, template ) "
                   "VALUES (:user_id, :fingerprint_template)");

    SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));

    query->bindValue(":fingerprint_template"  , crypto.encryptToByteArray(detailsOfUser->fingerprint_template));
    query->bindValue(":user_id"  ,temp_id );

    if(query->exec())
    {
        qDebug()<< "Database::addSystemUser fingerprint: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::addSystemUser fingerprint: query error - "
                << query->lastError();

        return cMinusFive;
    }

}


/*!
 * \brief Database::addRecipient
 * \param RecipientsDetails*
 * \return 0 recipient added successfully
 *         -1 db conn false
 *         -2 Enter all required fields
 *         -3 Fingerprint not enrolled
 *         -4 query error
 */
int Database::addRecipient(RecipientsDetails* detailsOfRecipient)
{
    // const int variables
    const int cZero       = 0;  // recipient added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // Enter all required fields
    const int cMinusThree = -3; // fingerprint not enrolled
    const int cMinusFour  = -4; // query error -  details
    const int cMinusFive  = -5; // query error -  fingerprint

    //int variable
    int temp_id = 0;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addRecipient: db_conn_state false";
        return cMinusOne;
    }

    // check minimum user details filled
    if(detailsOfRecipient->name.trimmed()     == "" ||
            detailsOfRecipient->surname.trimmed()  == "" ||
            detailsOfRecipient->school_id          == 0   ||
            detailsOfRecipient->region_id          == 0){
        qDebug()<< "Database::addRecipient: Enter all required fields";
        return cMinusTwo;
    }

    // check fingerprint enrolled
    if(detailsOfRecipient->fingerprint_template == NULL && detailsOfRecipient->fingerprint_enrolled){
        qDebug()<< "Database::addRecipient: recipient's fingerprint not enrolled";
        return cMinusThree;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO recipients (name, surname, gender, birth_date, address,"
                   " region_id,school_id, fingerprint_enrolled,contact_number,id_number,recipient_type ) "
                   "VALUES "
                   "(:name, :surname, :gender, :birth_date, :address, :region_id, :school_id,"
                   ":fingerprint_enrolled,:contact,:id_number,:recipient_type)");

    query->bindValue(":name"                  , detailsOfRecipient->name);
    query->bindValue(":surname"               , detailsOfRecipient->surname);
    query->bindValue(":gender"                , detailsOfRecipient->gender);
    query->bindValue(":birth_date"            , detailsOfRecipient->birth_date);
    query->bindValue(":address"               , detailsOfRecipient->address);
    query->bindValue(":region_id"             , detailsOfRecipient->region_id);
    query->bindValue(":school_id"             , detailsOfRecipient->school_id);
    query->bindValue(":fingerprint_enrolled"  , detailsOfRecipient->fingerprint_enrolled);
    query->bindValue(":contact"               , detailsOfRecipient->contact_number);
    query->bindValue(":id_number"             , detailsOfRecipient->id_number);
    query->bindValue(":recipient_type"        , detailsOfRecipient->recipient_type);


    if(query->exec())
    {
        qDebug()<< "Database::addRecipient details: successful";

        // if fp is not enrolled..its okay
        if(!detailsOfRecipient->fingerprint_enrolled)
            return cZero;
    }
    else
    {
        qDebug()<< "Database::addRecipient details: query error - "
                << query->lastError();

        return cMinusFour;
    }

    query->prepare("SELECT id FROM recipients order by id asc");


    if(query->exec())
    {
        while (query->next())
        {
            temp_id = query->value(query->record().indexOf("id")).toInt();
        }
    }
    else
    {
        qDebug()<< "Database::addRecipient fingerprint: MAX(id) query error - "
                << query->lastError();

        return cMinusFive;
    }

    query->prepare("INSERT INTO recipients_templates ( user_id, template ) "
                   "VALUES (:rec_id, :fingerprint_template)");

    SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));

    query->bindValue(":fingerprint_template"  , crypto.encryptToByteArray(detailsOfRecipient->fingerprint_template));
    query->bindValue(":rec_id"  ,temp_id );

    if(query->exec())
    {
        qDebug()<< "Database::addRecipient fingerprint: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::addRecipient fingerprint: query error - "
                << query->lastError();

        return cMinusFive;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////




/*!
 * \brief Database::addStaff
 * \param StaffDetails*
 * \return 0 staff added successfully
 *         -1 db conn false
 *         -2 Enter all required fields
 *         -3 Fingerprint not enrolled
 *         -4 query error
 */
int Database::addStaff(StaffDetails* detailsOfStaff)
{
    // const int variables
    const int cZero       = 0;  // recipient added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // Enter all required fields
    const int cMinusThree = -3; // fingerprint not enrolled
    const int cMinusFour  = -4; // query error -  details
    const int cMinusFive  = -5; // query error -  fingerprint

    //int variable
    int temp_id = 0;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addStaff: db_conn_state false";
        return cMinusOne;
    }

    // check minimum user details filled
    if(detailsOfStaff->username.trimmed()     == ""){
        qDebug()<< "Database::addStaff: Enter required field";
        return cMinusTwo;
    }

    // check fingerprint enrolled
    if(detailsOfStaff->fingerprint_template == NULL && detailsOfStaff->fingerprint_enrolled){
        qDebug()<< "Database::addStaff: staff's fingerprint not enrolled";
        return cMinusThree;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO enrollment (username,fingerprint_enrolled) "
                   "VALUES "
                   "(:username,:fingerprint_enrolled)");

    query->bindValue(":username"              , detailsOfStaff->username);
    query->bindValue(":fingerprint_enrolled"  , detailsOfStaff->fingerprint_enrolled);



    if(query->exec())
    {
        qDebug()<< "Database::addStaff details: successful";

        // if fp is not enrolled..its okay
        if(!detailsOfStaff->fingerprint_enrolled)
            return cZero;
    }
    else
    {
        qDebug()<< "Database::addStaff details: query error - "
                << query->lastError();

        return cMinusFour;
    }

    query->prepare("SELECT id FROM enrollment order by id asc");


    if(query->exec())
    {
        while (query->next())
        {
            temp_id = query->value(query->record().indexOf("id")).toInt();
        }
    }
    else
    {
        qDebug()<< "Database::addStaff fingerprint: MAX(id) query error - "
                << query->lastError();

        return cMinusFive;
    }

    query->prepare("INSERT INTO enrollment_templates ( user_id, template ) "
                   "VALUES (:rec_id, :fingerprint_template)");

    SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));

    query->bindValue(":fingerprint_template"  , crypto.encryptToByteArray(detailsOfStaff->fingerprint_template));
    query->bindValue(":rec_id"  ,temp_id );

    if(query->exec())
    {
        qDebug()<< "Database::addStaff fingerprint: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::addStaff fingerprint: query error - "
                << query->lastError();

        return cMinusFive;
    }
}


/*!
 * \brief Database::editStaff
 * \param StaffDetails*
 * \param StafftIndex
 * \return 0 Staff editted successfully
 *         -1 db conn false
 *         -2 user's username not entered
 *         -3 fingerprint not enrolled
 *         -4 query error -  details
 *         -5 query error -  fingerprint
 */
int Database::editStaff( StaffDetails* detailsOfStaff,
                             int                staffIndex)
{
    // const int variables
    const int cZero       = 0;  // Staff added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // user's username not entered
    const int cMinusThree = -3; // fingerprint not enrolled
    const int cMinusFour  = -4; // query error -  details
    const int cMinusFive  = -5; // query error -  fingerprint


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::editStaff: db_conn_state false";
        return cMinusOne;
    }

    // check minimum user details filled
    if(detailsOfStaff->username.trimmed()     == ""){
        qDebug()<< "Database::editStaff: Staff's username not entered";
        return cMinusTwo;
    }

    // check fingerprint enrolled
    if(detailsOfStaff->fingerprint_template == NULL){
        qDebug()<< "Database::editStaff:Staff's fingerprint not enrolled";
        //return cMinusThree;
    }


    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("UPDATE enrollment set username = (:name), surname = (:surname), fingerprint_enrolled = (:fingerprint_enrolled)"
                   " WHERE id = (:id) ");

    query->bindValue(":name"                  , detailsOfStaff->username);
    query->bindValue(":fingerprint_enrolled"  , detailsOfStaff->fingerprint_enrolled);
    query->bindValue(":id"                    , staffIndex);

    if(query->exec())
    {
        qDebug()<< "Database::editStaff details: successful";

    }
    else
    {
        qDebug()<< "Database::editStaff details: query error - "
                << query->lastError();

        return cMinusFour;
    }

    // update template only if it was uploaded
    if(detailsOfStaff->fingerprint_template != NULL){

        query->prepare("DELETE recipients_templates WHERE user_id = (:id) ");

        SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));

        query->bindValue(":id",staffIndex);

        qDebug()<<"staffIndex - "<<staffIndex<< "  name - "<<detailsOfStaff->username;

        // try inserting it
        query->prepare("INSERT INTO enrollment_templates ( user_id, template ) "
                       "VALUES (:rec_id, :fingerprint_template)");

        query->bindValue(":fingerprint_template"  , crypto.encryptToByteArray(detailsOfStaff->fingerprint_template));
        query->bindValue(":rec_id"  ,staffIndex);

        if(query->exec())
        {
            qDebug()<< "Database::editStaff fingerprint: UPDATE successful";
            return cZero;
        }
        else
        {
            qDebug()<< "Database::editStaff fingerprint: query error - "
                    << query->lastError();
        }
        return cMinusFive;
    }
    else
    {
        return cZero;
    }
}

/*!
 * \brief Database::deleteStaff
 * \param StaffIndex
 * \return 0 Staff deleted successfully
 *         -1 db conn false
 *         -2 query error - details
 *         -3 query error - template
 */
int Database::deleteStaff(int staffIndex)
{
    // const int variables
    const int cZero       = 0;  // system user deleted successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error - details
    const int cMinusThree = -3; // query error - template

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::deleteStaff: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("DELETE FROM enrolment WHERE id = (:id) ");
    query->bindValue(":id", staffIndex);

    if(query->exec())
    {
        qDebug()<< "Database::deleteStaff details: successful";
    }
    else
    {
        qDebug()<< "Database::deleteStaff details: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    query->prepare("DELETE FROM enrollment_templates WHERE id = (:id) ");
    query->bindValue(":id", staffIndex);

    if(query->exec())
    {
        qDebug()<< "Database::deleteStaff template: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::deleteStaff template: query error - "
                << query->lastError();

        return cMinusThree;
    }
}



//////////////////////////////////////////////////////////////////////////////////////////////


/*!
 * \brief Database::addRegion
 * \param RegionDetails*
 * \return 0 region added successfully/exists
 *         -1 db conn false
 *         -2 District not entered
 *         -3 query error
 */
int Database::addRegion(RegionDetails* detailsOfRegion)
{

    // check for duplicate
    int temp_id = 0;
    this->getIdByDistricName(detailsOfRegion->district,temp_id);
    if(temp_id != 0)
    {
        qDebug()<<detailsOfRegion->district<<" exists";
        return 0;
    }

    // const int variables
    const int cZero       = 0;  // region added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // Enter all required fields
    const int cMinusThree = -3; // query error -  details

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addRegion: db_conn_state false";
        return cMinusOne;
    }

    // check minimum region details filled
    if(detailsOfRegion->district.trimmed()     == "" || detailsOfRegion->region.trimmed()     == ""){
        qDebug()<< "Database::addRegion: Enter all required fields";
        return cMinusTwo;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO regions (district, education_coordinator, ec_contact_number, dsd_coordinator, dsd_contact_number, region) "
                   "VALUES "
                   "(:district, :education_coordinator, :ec_contact_number, :dsd_coordinator, :dsd_contact_number, :region)");

    query->bindValue(":district"              , detailsOfRegion->district);
    query->bindValue(":education_coordinator" , (detailsOfRegion->education_coord.trimmed()     == "" ? "None" : detailsOfRegion->education_coord));
    query->bindValue(":ec_contact_number"     , (detailsOfRegion->education_coord_tel.trimmed() == "" ? "None" : detailsOfRegion->education_coord_tel));
    query->bindValue(":dsd_coordinator"       , (detailsOfRegion->dsd_coord.trimmed()           == "" ? "None" : detailsOfRegion->dsd_coord));
    query->bindValue(":dsd_contact_number"    , (detailsOfRegion->education_coord_tel.trimmed() == "" ? "None" : detailsOfRegion->education_coord_tel));
    query->bindValue(":region"                , detailsOfRegion->region);


    if(query->exec())
    {
        qDebug()<< "Database::addRegion details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::addRegion details: query error - "
                << query->lastError();

        return cMinusThree;
    }
}

/*!
 * \brief Database::addSchool
 * \param SchoolDetails*
 * \return 0 school added successfully
 *         -1 db conn false
 *         -2 School not entered
 *         -3 query error
 */
int Database::addSchool(SchoolDetails* detailsOfSchool)
{
    // const int variables
    const int cZero       = 0;  // school added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // Enter all required fields
    const int cMinusThree = -3; // query error -  details

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addSchool: db_conn_state false";
        return cMinusOne;
    }

    // check minimum school details filled
    if(detailsOfSchool->name.trimmed()  == "" ||
            detailsOfSchool->region_id       == 0){
        qDebug()<< "Database::addSchool: Enter all required fields";
        return cMinusTwo;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO schools (region_id, name, address, contact_person_name, contact_person_surname, no_of_learners,no_of_learners_enrolled ) "
                   "VALUES "
                   "(:region_id, :name, :address, :contact_person_name, :contact_person_surname, :no_of_learners, :no_of_learners_enrolled )");

    query->bindValue(":region_id"               , detailsOfSchool->region_id);
    query->bindValue(":name"                    , detailsOfSchool->name.trimmed());
    query->bindValue(":address"                 , (detailsOfSchool->address.trimmed()                == "" ? "None" : detailsOfSchool->address));
    query->bindValue(":contact_person_name"     , (detailsOfSchool->contact_person_name.trimmed()    == "" ? "None" : detailsOfSchool->contact_person_name));
    query->bindValue(":contact_person_surname"  , (detailsOfSchool->contact_person_surname.trimmed() == "" ? "None" : detailsOfSchool->contact_person_surname));
    query->bindValue(":no_of_learners"          , detailsOfSchool->num_of_learners);
    query->bindValue(":no_of_learners_enrolled" , detailsOfSchool->num_of_learners_enrolled);

    if(query->exec())
    {
        qDebug()<< "Database::addSchool details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::addSchool details: query error - "
                << query->lastError();

        return cMinusThree;
    }
}


/*!
 * \brief Database::addStockItems
 * \param description
 * \param count
 * \return 0 items added successfully
 *         -1 db conn false
 *         -2 Items not entered
 *         -3 query error
 */
int Database::addStockItems(QString description,
                            double  count)
{
    // const int variables
    const int cZero       = 0;  // items added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error

    UserDetails current_user;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addStockItems: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO stock_items (description,current_count,last_update_date, updated_by) "
                   "VALUES "
                   "(:id, :description, :current_count, :last_update_date,:updated_by) ");
    this->getUserDetailsById(current_user,SystemUser::Instance()->getId());

    query->bindValue(":description"       ,description);
    query->bindValue(":current_count"     ,count);
    query->bindValue(":last_update_date"  ,QDateTime::currentDateTime().date());
    query->bindValue(":updated_by"        ,QString("%1 %2").arg(current_user.name).arg(current_user.surname));


    if(query->exec())
    {
        qDebug()<< "Database::addStockItems details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::addStockItems details: query error - "
                << query->lastError();

        return cMinusTwo;
    }
}


/*!
 * \brief getDignityPackCounts
 * \param count
 * \param action
 * \return 0 packs retrieved successfully
 *         -1 db conn false
 *         -2 count not retrieved
 *         -3 query error
 */
int Database::getDignityPackCounts(double& count)
{
    // const int variables
    const int cOne      = 1;  // first id
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getDignityPackCounts: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM stock_items WHERE id = (:ind)");
    query->bindValue(":ind", cOne);

    count = 0;

    if(query->exec())
    {
        while (query->next()){
            count = query->value(query->record().indexOf("current_count")).toDouble();
            break;
        }
    }
    else
    {
        qDebug()<< "Database::getDignityPackCounts: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}

/*!
 * \brief getDignityPackLimitDetails
 * \param count
 * \param action
 * \return 0 packs limit count and email retreived successfully
 *         -1 db conn false
 *         -2 limit/email not retrieved
 *         -3 query error
 */
int Database::getDignityPackLimitDetails(double&  limit,
                                         QString& email)
{
    // const int variables
    const int cOne      = 1;  // first id
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getDignityPackLimitDetails: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM stock_settings WHERE id = (:ind)");
    query->bindValue(":ind", cOne);

    limit = 0;
    email.clear();

    if(query->exec())
    {
        while (query->next()){
            limit = query->value(query->record().indexOf("limit_count")).toDouble();
            email = query->value(query->record().indexOf("alert_email_address")).toString();
            break;
        }
    }
    else
    {
        qDebug()<< "Database::getDignityPackLimitDetails: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}

/*!
 * \brief updateDignityPacks
 * \param count
 * \param action
 * \return 0 packs updated successfully
 *         -1 db conn false
 *         -2 Items not entered
 *         -3 query error
 */
int Database::updateDignityPacks(double             count,
                                 QString            comment,
                                 dignityPackAction  action)
{
    // const int variables
    const int cOne        = 1;
    const int cZero       = 0;  // items added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error

    UserDetails current_user;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::addStockItems: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("UPDATE stock_items set current_count = (:current_count), "
                   "last_update_date  = (:last_update_date), updated_by = (:updated_by), "
                   "comment = (:comment) WHERE id = (:id)");

    this->getUserDetailsById(current_user,SystemUser::Instance()->getId());

    double cnt = 0;
    this->getDignityPackCounts(cnt);

    query->bindValue(":id"                ,cOne);
    query->bindValue(":current_count"     ,((action == ADD)? (cnt + count) : (cnt - count)));
    query->bindValue(":last_update_date"  ,QDateTime::currentDateTime().date());
    query->bindValue(":updated_by"        ,QString("%1 %2").arg(current_user.name).arg(current_user.surname));
    query->bindValue(":comment"           ,comment);

    if(query->exec())
    {
        qDebug()<< "Database::updateDignityPacks details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::updateDignityPacks details: query error - "
                << query->lastError();

        return cMinusTwo;
    }
}


/*!
 * \brief updateStockSettings
 * \param limit
 * \param email
 * \return 0 stock settings updated successfully
 *         -1 db conn false
 *         -2 Items not entered
 *         -3 query error
 */
int Database::updateStockSettings(double  limit,
                                  QString email)
{
    // const int variables
    const int cOne        = 1;
    const int cZero       = 0;  // settings updated successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error

    // Email validity check
    QRegExp mailREX("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    mailREX.setCaseSensitivity(Qt::CaseInsensitive);
    mailREX.setPatternSyntax(QRegExp::RegExp);

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::updateStockSettings: db_conn_state false";
        return cMinusOne;
    }

    QSqlQuery* query = new QSqlQuery(db_handler_);

    if(limit > 0 && !email.isEmpty()){

        query->prepare("UPDATE stock_settings set limit_count = (:limit), alert_email_address = (:email)");

        query->bindValue(":limit" ,limit);
        query->bindValue(":email" ,email);

    } else if(limit > 0){

        query->prepare("UPDATE stock_settings set limit_count = (:limit)");
        query->bindValue(":limit" ,limit);

    }else if(!email.isEmpty() && mailREX.exactMatch(email)){

        query->prepare("UPDATE stock_settings set alert_email_address = (:email)");
        query->bindValue(":email" ,email);
    }
    else
    {
        return cMinusTwo;
    }

    if(query->exec())
    {
        qDebug()<< "Database::updateStockSettings details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::updateStockSettings details: query error - "
                << query->lastError();

        return cMinusTwo;
    }
}




/*!
 * \brief Database::editSystemUser
 * \param detailsOfUser*
 * \param userIndex
 * \return 0 system user editted successfully
 *         -1 db conn false
 *         -2 user's Name or Surname not entered
 *         -3 fingerprint not enrolled
 *         -4 query error -  details
 *         -5 query error -  fingerprint
 */
int Database::editSystemUser( UserDetails*  detailsOfUser,
                              int           userIndex)
{
    // const int variables
    // const int variables
    const int cZero       = 0;  // recipient added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // user's Name or Surname not entered
    const int cMinusThree = -3; // fingerprint not enrolled
    const int cMinusFour  = -4; // query error -  details
    const int cMinusFive  = -5; // query error -  fingerprint


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::editSystemUser: db_conn_state false";
        return cMinusOne;
    }

    // check minimum user details filled
    if(detailsOfUser->name.trimmed()     == "" ||
            detailsOfUser->surname.trimmed()  == ""){
        qDebug()<< "Database::editSystemUser: user's Name or Surname not entered";
        return cMinusTwo;
    }

    // check fingerprint enrolled
    if(detailsOfUser->fingerprint_template == NULL){
        qDebug()<< "Database::editRecipient: recipient's fingerprint not enrolled";
        //return cMinusThree;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("UPDATE users set name = (:name), surname = (:surname), user_type = (:user_type), "
                   "status = (:status), email_address = (:email_address), contact_number = (:contact_number)"
                   ", fingerprint_enrolled = (:fingerprint_enrolled) WHERE id = (:id) ");

    query->bindValue(":id"                    , userIndex);
    query->bindValue(":name"                  , detailsOfUser->name);
    query->bindValue(":surname"               , detailsOfUser->surname);
    query->bindValue(":user_type"             , (int)detailsOfUser->user_type);
    query->bindValue(":status"                , (int)detailsOfUser->account_status);
    query->bindValue(":email_address"         , detailsOfUser->email_address);
    query->bindValue(":contact_number"        , detailsOfUser->contact_number);
    query->bindValue(":fingerprint_enrolled"  , detailsOfUser->fingerprint_enrolled);



    if(query->exec())
    {
        qDebug()<< "Database::editSystemUser: successful";
    }
    else
    {
        qDebug()<< "Database::editSystemUser: query error - "
                << query->lastError();

        return cMinusThree;
    }

    // update template only if it was uploaded
    if(detailsOfUser->fingerprint_template != NULL){

        query->prepare("DELETE users_templates WHERE user_id = (:id) ");

        SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));

        query->bindValue(":id",userIndex);

        // try inserting it
        query->prepare("INSERT INTO users_templates ( user_id, template ) "
                       "VALUES (:rec_id, :fingerprint_template)");

        query->bindValue(":fingerprint_template"  , crypto.encryptToByteArray(detailsOfUser->fingerprint_template));
        query->bindValue(":rec_id"  ,userIndex);

        if(query->exec())
        {
            qDebug()<< "Database::editSystemUser fingerprint: UPDATE successful";
            return cZero;
        }
        else
        {
            qDebug()<< "Database::editSystemUser fingerprint: query error - "
                    << query->lastError();
        }
        return cMinusFive;
    }
    else
    {
        return cZero;
    }
}


/*!
 * \brief Database::editRecipient
 * \param RecipientsDetails*
 * \param recipientIndex
 * \return 0 recipient editted successfully
 *         -1 db conn false
 *         -2 user's Name or Surname not entered
 *         -3 fingerprint not enrolled
 *         -4 query error -  details
 *         -5 query error -  fingerprint
 */
int Database::editRecipient( RecipientsDetails* detailsOfRecipient,
                             int                recipientIndex)
{
    // const int variables
    const int cZero       = 0;  // recipient added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // user's Name or Surname not entered
    const int cMinusThree = -3; // fingerprint not enrolled
    const int cMinusFour  = -4; // query error -  details
    const int cMinusFive  = -5; // query error -  fingerprint


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::editRecipient: db_conn_state false";
        return cMinusOne;
    }

    // check minimum user details filled
    if(detailsOfRecipient->name.trimmed()     == "" ||
            detailsOfRecipient->surname.trimmed()  == ""){
        qDebug()<< "Database::editRecipient: recipient's Name or Surname not entered";
        return cMinusTwo;
    }

    // check fingerprint enrolled
    if(detailsOfRecipient->fingerprint_template == NULL){
        qDebug()<< "Database::editRecipient: recipient's fingerprint not enrolled";
        //return cMinusThree;
    }


    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("UPDATE recipients set name = (:name), surname = (:surname), gender = (:gender), "
                   "birth_date = (:birth_date), address = (:address),"
                   "region_id = (:region_id),school_id = (:school_id), fingerprint_enrolled = (:fingerprint_enrolled), "
                   "contact_number = (:contact), id_number = (:id_number), recipient_type = (:recipient_type) "
                   " WHERE id = (:id) ");

    query->bindValue(":name"                  , detailsOfRecipient->name);
    query->bindValue(":surname"               , detailsOfRecipient->surname);
    query->bindValue(":gender"                , detailsOfRecipient->gender);
    query->bindValue(":birth_date"            , detailsOfRecipient->birth_date);
    query->bindValue(":address"               , detailsOfRecipient->address);
    query->bindValue(":region_id"             , detailsOfRecipient->region_id);
    query->bindValue(":school_id"             , detailsOfRecipient->school_id);
    query->bindValue(":fingerprint_enrolled"  , detailsOfRecipient->fingerprint_enrolled);
    query->bindValue(":contact"               , detailsOfRecipient->contact_number);
    query->bindValue(":id_number"             , detailsOfRecipient->id_number);
    query->bindValue(":recipient_type"        , detailsOfRecipient->recipient_type);
    query->bindValue(":id"                    , recipientIndex);

    if(query->exec())
    {
        qDebug()<< "Database::editRecipient details: successful";

    }
    else
    {
        qDebug()<< "Database::editRecipient details: query error - "
                << query->lastError();

        return cMinusFour;
    }

    // update template only if it was uploaded
    if(detailsOfRecipient->fingerprint_template != NULL){

        query->prepare("DELETE recipients_templates WHERE user_id = (:id) ");

        SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));

        query->bindValue(":id",recipientIndex);

        qDebug()<<"recipientIndex - "<<recipientIndex<< "  name - "<<detailsOfRecipient->name;

        // try inserting it
        query->prepare("INSERT INTO recipients_templates ( user_id, template ) "
                       "VALUES (:rec_id, :fingerprint_template)");

        query->bindValue(":fingerprint_template"  , crypto.encryptToByteArray(detailsOfRecipient->fingerprint_template));
        query->bindValue(":rec_id"  ,recipientIndex);

        if(query->exec())
        {
            qDebug()<< "Database::editRecipient fingerprint: UPDATE successful";
            return cZero;
        }
        else
        {
            qDebug()<< "Database::editRecipient fingerprint: query error - "
                    << query->lastError();
        }
        return cMinusFive;
    }
    else
    {
        return cZero;
    }
}




/*!
 * \brief Database::deleteSystemUser
 * \param userIndex
 * \return 0 system user deleted successfully
 *         -1 db conn false
 *         -2 query error - details
 *         -3 query error - template
 */
int Database::deleteSystemUser(int userIndex)
{
    // const int variables
    const int cZero       = 0;  // system user added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error - details
    const int cMinusThree = -3; // query error - template
    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::deleteSystemUser: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("DELETE FROM users WHERE id = (:id) ");
    query->bindValue(":id", userIndex);

    if(query->exec())
    {
        qDebug()<< "Database::deleteSystemUser: successful";
    }
    else
    {
        qDebug()<< "Database::deleteSystemUser: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    query->prepare("DELETE FROM users_templates WHERE id = (:id) ");
    query->bindValue(":id", userIndex);

    if(query->exec())
    {
        qDebug()<< "Database::deleteSystemUser template: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::deleteSystemUser template: query error - "
                << query->lastError();

        return cMinusThree;
    }
}

/*!
 * \brief Database::deleteRecipient
 * \param recipientIndex
 * \return 0 recipient deleted successfully
 *         -1 db conn false
 *         -2 query error - details
 *         -3 query error - template
 */
int Database::deleteRecipient(int recipientIndex)
{
    // const int variables
    const int cZero       = 0;  // system user added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error - details
    const int cMinusThree = -3; // query error - template

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::deleteRecipient: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("DELETE FROM recipients WHERE id = (:id) ");
    query->bindValue(":id", recipientIndex);

    if(query->exec())
    {
        qDebug()<< "Database::deleteRecipient details: successful";
    }
    else
    {
        qDebug()<< "Database::deleteRecipient details: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    query->prepare("DELETE FROM recipients_templates WHERE id = (:id) ");
    query->bindValue(":id", recipientIndex);

    if(query->exec())
    {
        qDebug()<< "Database::deleteRecipient template: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::deleteRecipient template: query error - "
                << query->lastError();

        return cMinusThree;
    }
}

/*!
 * \brief Database::getAllSystemUsers
 * \param users_list
 * \return
 */
int Database::getAllSystemUsers(QList<QPair<UserDetails ,int> >& users_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // temp user details
    int                     id;
    UserDetails             user_details;
    QPair<UserDetails,int>  user_details_set;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getAllSystemUsers: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    // check that this sys user can add this user type
    if(SystemUser::Instance()->getUserType() == SuperAdmin)
        query->prepare("SELECT * FROM users order by user_type asc");
    else
        query->prepare("SELECT * FROM users where user_type != '1' order by user_type asc");

    if(query->exec())
    {
        while (query->next())
        {
            id                           = query->value(query->record().indexOf("id")).toInt();
            user_details.name            = query->value(query->record().indexOf("name")).toString();
            user_details.surname         = query->value(query->record().indexOf("surname")).toString();
            user_details.email_address   = query->value(query->record().indexOf("email_address")).toString();
            user_details.contact_number  = query->value(query->record().indexOf("contact_number")).toString();
            user_details.user_type       = (userType)query->value(query->record().indexOf("user_type")).toInt();
            user_details.account_status  = (accStatus)query->value(query->record().indexOf("status")).toInt();

            user_details_set.first       = user_details;
            user_details_set.second      = id;
            users_list.append(user_details_set);

        }

    }
    else
    {
        qDebug()<< "Database::getAllSystemUsers: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}


/*!
 * \brief Database::getAllRecipients
 * \param recipients_list
 * \return
 */
int Database::getAllRecipients(QList<RecipientsDetails>& recipients_details)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    RecipientsDetails temp_recipient_details;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getAllRecipients: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM recipients order by name asc");

    if(query->exec())
    {
        while (query->next())
        {
            temp_recipient_details.id                    = query->value(query->record().indexOf("id")).toInt();
            temp_recipient_details.name                  = query->value(query->record().indexOf("name")).toString();
            temp_recipient_details.surname               = query->value(query->record().indexOf("surname")).toString();
            temp_recipient_details.gender                = query->value(query->record().indexOf("gender")).toString();
            temp_recipient_details.recipient_type        = (recipientType)query->value(query->record().indexOf("recipient_type")).toInt();
            temp_recipient_details.birth_date            = query->value(query->record().indexOf("birth_date")).toDate();
            temp_recipient_details.address               = query->value(query->record().indexOf("address")).toString();
            temp_recipient_details.contact_number        = query->value(query->record().indexOf("contact_number")).toString();
            temp_recipient_details.id_number             = query->value(query->record().indexOf("id_number")).toString();
            temp_recipient_details.region_id             = query->value(query->record().indexOf("region_id")).toInt();
            temp_recipient_details.school_id             = query->value(query->record().indexOf("school_id")).toInt();
            temp_recipient_details.fingerprint_enrolled  = query->value(query->record().indexOf("fingerprint_enrolled")).toBool();

            recipients_details.append(temp_recipient_details);
        }
    }
    else
    {
        qDebug()<< "Database::getAllRecipients: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}

int Database::getRecipientById(int id,
                               RecipientsDetails &recipients_details){
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getRecipientById: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM recipients WHERE id = (:id)");
    query->bindValue(":id", id);

    if(query->exec())
    {
        while (query->next())
        {
            recipients_details.id                    = query->value(query->record().indexOf("id")).toInt();
            recipients_details.name                  = query->value(query->record().indexOf("name")).toString();
            recipients_details.surname               = query->value(query->record().indexOf("surname")).toString();
            recipients_details.gender                = query->value(query->record().indexOf("gender")).toString();
            recipients_details.recipient_type        = (recipientType)query->value(query->record().indexOf("recipient_type")).toInt();
            recipients_details.contact_number        = query->value(query->record().indexOf("contact_number")).toString();
            recipients_details.id_number             = query->value(query->record().indexOf("id_number")).toString();
            recipients_details.birth_date            = query->value(query->record().indexOf("birth_date")).toDate();
            recipients_details.address               = query->value(query->record().indexOf("address")).toString();
            recipients_details.region_id             = query->value(query->record().indexOf("region_id")).toInt();
            recipients_details.school_id             = query->value(query->record().indexOf("school_id")).toInt();
            recipients_details.fingerprint_enrolled  = query->value(query->record().indexOf("fingerprint_enrolled")).toBool();
        }
    }
    else
    {
        qDebug()<< "Database::getRecipientById: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}


/*!
 * \brief Database::getAllRecipientsRegions
 * \param regions_list
 * \return 0 regions retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 **/
int Database::getAllRecipientsRegions(QList<RegionDetails>& regions_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error
    RegionDetails temp_region_details;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getAllRecipientsRegions: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM regions order by district asc");

    if(query->exec())
    {
        while (query->next())
        {
            temp_region_details.region_id            = query->value(query->record().indexOf("id")).toInt();
            temp_region_details.district             = query->value(query->record().indexOf("district")).toString();
            temp_region_details.dsd_coord            = query->value(query->record().indexOf("dsd_coordinator")).toString();
            temp_region_details.dsd_coord_tel        = query->value(query->record().indexOf("dsd_contact_number")).toString();
            temp_region_details.education_coord      = query->value(query->record().indexOf("education_coordinator")).toString();
            temp_region_details.education_coord_tel  = query->value(query->record().indexOf("ec_contact_number")).toString();

            regions_list.append(temp_region_details);
        }
    }
    else
    {
        qDebug()<< "Database::getAllRecipientsRegions: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

/*!
 * \brief Database::getSchoolsByDistricId
 * \param regions_list
 * \return 0 schools retrived successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getSchoolsByDistricId(QList< QPair<QString, int> >& schools_list,
                                    int                           districtId)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error
    QPair<QString, int> temp_schools_list;
    schools_list.clear();

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getSchoolsByDistricId: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT id,name FROM schools where region_id = (:index) order by name asc");
    query->bindValue(":index", districtId);

    if(query->exec())
    {
        while (query->next())
        {
            temp_schools_list.first   = query->value(query->record().indexOf("name")).toString();
            temp_schools_list.second  = query->value(query->record().indexOf("id")).toInt();

            schools_list.append(temp_schools_list);
        }
    }
    else
    {
        qDebug()<< "Database::getSchoolsByDistricId: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}


/*!
 * \brief Database::getSchoolNameById
 * \param schools_list
 * \param id
 * \return 0 school retrived successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getSchoolNameById(QString& schoolName,
                                int      id)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getSchoolsByDistricId: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT name FROM schools WHERE id = (:id)");
    query->bindValue(":id", id);

    if(query->exec())
    {
        while (query->next())
            schoolName = query->value(query->record().indexOf("name")).toString();

    }
    else
    {
        qDebug()<< "Database::getSchoolsByDistricId: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

/*!
 * \brief getNumberOfSchools
 * \param schools_count
 * \return 0 school count retrived successfully
 *         -1 db conn false
 *         -2 query error
 */
int Database::getNumberOfSchools(int& schools_count)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getNumberOfSchools: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT COUNT(*) FROM schools");

    if(query->exec())
    {
        while (query->next())
            schools_count = query->value(0).toInt();
    }
    else
    {
        qDebug()<< "Database::getNumberOfSchools: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}


/*!
 * \brief getSchoolDetailsByIdAndRegionId
 * \param school
 * \param school_id
 * \param region_id
 * \return 0 school retrived successfully
 *         -1 db conn false
 *         -2 query error
 */
int Database::getSchoolDetailsByIdAndRegionId(SchoolDetails&  school,
                                              int             school_id,
                                              int             region_id)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getSchoolDetailsByIdAndRegionId: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM schools WHERE id = (:id) AND region_id = (:r_id)");
    query->bindValue(":id"    , school_id);
    query->bindValue(":r_id"  , region_id);

    if(query->exec())
    {
        while (query->next())
        {
            school.id                       = query->value(query->record().indexOf("id")).toInt();
            school.region_id                = query->value(query->record().indexOf("region_id")).toInt();
            school.name                     = query->value(query->record().indexOf("name")).toString();
            school.address                  = query->value(query->record().indexOf("address")).toString();
            school.contact_person_name      = query->value(query->record().indexOf("contact_person_name")).toString();
            school.contact_person_surname   = query->value(query->record().indexOf("contact_person_surname")).toString();
            school.num_of_learners          = query->value(query->record().indexOf("no_of_learners")).toInt();
            school.num_of_learners_enrolled = query->value(query->record().indexOf("no_of_learners_enrolled")).toInt();
        }
    }
    else
    {
        qDebug()<< "Database::getSchoolDetailsByIdAndRegionId: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}



/*!
 * \brief Database::getDistricNametById
 * \param schools_list
 * \param id
 * \return 0 district name retrieved successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getDistricNameById(QString& district_name,
                                 int      ind){
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getDistricNametById: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM regions WHERE id = (:ind)");
    query->bindValue(":ind", ind);

    if(query->exec())
    {
        while (query->next()){
            district_name = query->value(query->record().indexOf("district")).toString();
        }


    }
    else
    {
        qDebug()<< "Database::getDistricNametById: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}



/*!
 * \brief Database::getRegionNameById
 * \param region_name
 * \param id
 * \return 0 region name retrived successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getRegionNameById(QString& region_name,
                                 int      ind)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getRegionNameById: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM regions WHERE id = (:ind)");
    query->bindValue(":ind", ind);

    if(query->exec())
    {
        while (query->next()){
            region_name = query->value(query->record().indexOf("region")).toString();
        }


    }
    else
    {
        qDebug()<< "Database::getRegionNameById: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}




/*!
 * \brief Database::getIdByDistricName
 * \param schools_list
 * \param id
 * \return 0 school retrived successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getIdByDistricName(QString district_name,
                                 int&      id)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getIdByDistricName: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM regions");

    if(query->exec())
    {
        while (query->next()) {
            if(query->value(query->record().indexOf("district")).toString().trimmed() == district_name.trimmed())
                id = query->value(query->record().indexOf("id")).toInt();
        }
    }
    else
    {
        qDebug()<< "Database::getIdByDistricName: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

int Database::trail(QString action)
{
    // const int variables
    const int cZero       = 0;  // audit trail added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // Actor or Action not entered
    const int cMinusThree = -3; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::trail: db_conn_state false";
        return cMinusOne;
    }

    // check minimum user details filled
    if(action.trimmed()  == ""){
        qDebug()<< "Database::trail: Action not entered";
        return cMinusTwo;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO audit_trail (actor, action, date, user_type ) "
                   "VALUES "
                   "(:actor, :action, :date, :user_type) ");

    QString names = SystemUser::Instance()->getName() + " " + SystemUser::Instance()->getSurname();
    query->bindValue(":actor"  ,names);
    query->bindValue(":user_type"  ,getUserTypeToString(SystemUser::Instance()->getUserType()));
    query->bindValue(":action",action);
    query->bindValue(":date",QDateTime::currentDateTime());


    if(query->exec())
    {
        qDebug()<< "Database::trail details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::trail details: query error - "
                << query->lastError();

        return cMinusThree;
    }

}

/*!
 * \brief log_distribution_activity
 * \param num_of_packs
 * \param received_by
 * \param recipient_id
 * \return 0 log ditribution activity successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::log_distribution_activity(int           num_of_packs,
                                        packReceiver  received_by,
                                        int           recipient_id)
{
    // const int variables
    const int cZero       = 0;  // log_distribution_activity added successfully
    const int cMinusOne   = -1; // db conn false
    const int cMinusTwo   = -2; // query error

    qDebug()<<"log_distribution_activity - packreceiver"<<getPackReceiverToString(received_by);
    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::log_distribution_activity: db_conn_state false";
        return cMinusOne;
    }

    // check credentials from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("INSERT INTO distribution_activity (recipient_id,user_id,accepted_by,date, packs_count) "
                   "VALUES "
                   "(:recipient_id, :user_id, :accepted_by, :date,:packs_cnt) ");

    query->bindValue(":recipient_id",recipient_id);
    query->bindValue(":user_id"     ,SystemUser::Instance()->getId());
    query->bindValue(":accepted_by" ,getPackReceiverToString(received_by));
    query->bindValue(":date"        ,QDateTime::currentDateTime().date());
    query->bindValue(":packs_cnt"   ,num_of_packs);


    if(query->exec())
    {
        qDebug()<< "Database::log_distribution_activity details: successful";
        return cZero;
    }
    else
    {
        qDebug()<< "Database::log_distribution_activity details: query error - "
                << query->lastError();

        return cMinusTwo;
    }

}



/*!
 * \brief Database::getSchoolNameById
 * \param schools_list
 * \param id
 * \return 0 school retrived successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getIdBySchoolName(QString school,
                                int&      id)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getIdBySchoolName: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT id FROM schools WHERE name = (:school)");
    query->bindValue(":school", school);

    if(query->exec())
    {
        while (query->next())
            id = query->value(query->record().indexOf("id")).toInt();
    }
    else
    {
        qDebug()<< "Database::getIdBySchoolName: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

/*!
 * \brief Database::getSchoolNameById
 * \param schools_list
 * \param id
 * \return 0 school retrived successfully
 *         -1 db conn false
 *         -2 query error
 **/
int Database::getAllRecipientsTemplates(QList<QPair<QByteArray, int> >& templates)
{
    //  // const int variables
    //  const int cZero     = 0;  // results found
    //  const int cMinusOne = -1; // db conn false
    //  const int cMinusTwo = -2; // query error

    //  // check status of db connection
    //  if(!db_conn_state_){
    //    qDebug()<< "Database::getIdBySchoolName: db_conn_state false";
    //    return cMinusOne;
    //  }

    //  // get user from db from db
    //  QSqlQuery* query = new QSqlQuery(db_handler_);

    //  query->prepare("SELECT id FROM schools WHERE name = (:school)");
    //  query->bindValue(":school", school);

    //  if(query->exec())
    //  {
    //    while (query->next())
    //    id = query->value(query->record().indexOf("id")).toInt();
    //  }
    //  else
    //  {
    //    qDebug()<< "Database::getIdBySchoolName: query error - "
    //            << query->lastError();

    //    return cMinusTwo;
    //  }
    //  return cZero;
}

int Database::getUserDetailsById(UserDetails& user_details,
                                 int      id)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getUserDetailsById: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM users WHERE id= (:id)");
    query->bindValue(":id",id);

    if(query->exec())
    {
        while (query->next())
        {
            user_details.id                   = query->value(query->record().indexOf("id")).toInt();
            user_details.name                 = query->value(query->record().indexOf("name")).toString();
            user_details.surname              = query->value(query->record().indexOf("surname")).toString();
            user_details.email_address        = query->value(query->record().indexOf("email_address")).toString();
            user_details.contact_number       = query->value(query->record().indexOf("contact_number")).toString();
            user_details.user_type            = (userType)query->value(query->record().indexOf("user_type")).toInt();
            user_details.account_status       = (accStatus)query->value(query->record().indexOf("status")).toInt();
            user_details.fingerprint_enrolled = query->value(query->record().indexOf("fingerprint_enrolled")).toBool();
        }

    }
    else
    {
        qDebug()<< "Database::getUserDetailsById: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}

/*!
 * \brief Database::getAllTemplateIds
 * \param template_ids_list
 * \return 0 templates ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 **/
int Database::getAllTemplateIds(QList<int> &template_ids_list,
                                userType type_of_user)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // clear container
    template_ids_list.clear();

    // temp table name
    QString temp_table_name;
    if(type_of_user == generalUser)
        temp_table_name = "users_templates";
    else
        temp_table_name = "recipients_templates";


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getAllTemplateIds: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT id FROM " + temp_table_name);

    if(query->exec())
    {
        while (query->next())
        {
            template_ids_list.append(query->value(query->record().indexOf("id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getAllTemplateIds: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

/*!
 * \brief getTemplateById
 * \param id
 * \param stored_template
 * \return 0 templates ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getTemplateById(int id,
                              QPair<QByteArray,int>& stored_template,
                              userType type_of_user){

    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // clear container
    stored_template.first.clear();
    QString temp_table_name;
    if(type_of_user == generalUser)
        temp_table_name = "users_templates";
    else
        temp_table_name = "recipients_templates";

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getTemplateById: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM " + temp_table_name + " WHERE id = (:id)");
    SimpleCrypt crypto(Q_UINT64_C(0x0c2ad4a4acb9f023));
    query->bindValue(":id", id);

    if(query->exec())
    {
        while (query->next())
        {
            stored_template.first   = crypto.decryptToByteArray(query->value(query->record().indexOf("template")).toByteArray());
            stored_template.second  = query->value(query->record().indexOf("user_id")).toInt();
        }
    }
    else
    {
        qDebug()<< "Database::getTemplateById: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}


/*!
 * \brief getRecipientsIdsByRegion
 * \param district_id
 * \param region_recipient_ids_list
 * \return 0 templates ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getRecipientsIdsByRegion(int          district_id,
                                       QList<int>&  region_recipient_ids_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getRecipientsIdsByRegion: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT id FROM recipients WHERE region_id = (:id)");
    query->bindValue(":id", district_id);

    if(query->exec())
    {
        while (query->next())
            region_recipient_ids_list.append(query->value(query->record().indexOf("id")).toInt());
    }
    else
    {
        qDebug()<< "Database::getRecipientsIdsByRegion: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}


/*!
 * \brief getRecipientsIdsBySchool
 * \param school_id
 * \param school_recipient_ids_list
 * \return 0 templates ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getRecipientsIdsBySchool(int         school_id,
                                       QList<int>& school_recipient_ids_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    school_recipient_ids_list.clear();

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getRecipientsIdsBySchool: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT id FROM recipients WHERE school_id = (:id) and recipient_type = '1'");
    query->bindValue(":id", school_id);

    if(query->exec())
    {
        while (query->next())
        {
            school_recipient_ids_list.append(query->value(query->record().indexOf("id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getRecipientsIdsBySchool: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

/*!
 * \brief Database::getTemplateIdsByRecipientId
 * \param recipient_id
 * \param template_ids_list
 * \return 0 templates ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getTemplateIdsByRecipientId(int recipient_id,
                                          QList<int> &template_ids_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // clear container
    template_ids_list.clear();

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getTemplateIdsByRecipientId: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT id FROM recipients_templates WHERE user_id = (:id)");
    query->bindValue(":id",recipient_id);

    if(query->exec())
    {
        while (query->next())
        {
            template_ids_list.append(query->value(query->record().indexOf("id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getTemplateIdsByRecipientId: query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}

/*!
 * \brief getDistributionActivityBy
 * \param from
 * \param to
 * \param dist_activity_list
 * \return 0 records ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getDistributionActivityDetailsByDate( QDate                               from,
                                                    QDate                               to,
                                                    QList<DistributionActivityDetails>& dist_activity_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    DistributionActivityDetails dist_activity;
    dist_activity_list.clear();

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getDistributionActivityByDate: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT * FROM distribution_activity WHERE date >= (:from) AND date <= (:to)");
    query->bindValue(":from",from);
    query->bindValue(":to",to);

    if(query->exec())
    {
        while (query->next())
        {
            dist_activity.id            = query->value(query->record().indexOf("id")).toInt();
            dist_activity.recipient_id  = query->value(query->record().indexOf("recipient_id")).toInt();
            dist_activity.user_id       = query->value(query->record().indexOf("user_id")).toInt();
            dist_activity.accepted_by   = getStringTopackReceiver(query->value(query->record().indexOf("accepted_by")).toString());
            dist_activity.date          = query->value(query->record().indexOf("date")).toDate();

            dist_activity_list.append(dist_activity);
        }
    }
    else
    {
        qDebug()<< "Database::getDistributionActivityByDate: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}

/*!
 * \brief getRegionDeliveriesByDate
 * \param from
 * \param to
 * \param reg_del_list
 * \return 0 records ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getRegionDeliveriesByDate( QDate                   from,
                               QDate                   to,
                               QList<RegionDeliveries> &region_delivery_list)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // temp variables
    int temp_total;
    QString temp_string;

    RegionDeliveries region_delivery;

    QList<int> rec_id_list;
    QList<int> del_list;
    QList<int> actual_rec_id_list;
    QList<int> region_id_list;
    QList<int> actual_rec_region_list;
    QList<int> actual_rec_del_count_list;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getRegionDeliveriesByDate: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT recipient_id, packs_count FROM distribution_activity "
                   "WHERE date >= (:from) AND date <= (:to) GROUP BY recipient_id");

    query->bindValue(":from",from);
    query->bindValue(":to"  ,to);

    if(query->exec())
    {
        while (query->next())
        {
            rec_id_list.append(query->value(query->record().indexOf("recipient_id")).toInt());
            del_list.append(query->value(query->record().indexOf("packs_count")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getRegionDeliveriesByDate: recipient_id, packs_count query error - "
                << query->lastError();

        return cMinusTwo;
    }

    query->prepare("SELECT id,region_id FROM recipients");

    if(query->exec())
    {
        while (query->next())
        {
            actual_rec_id_list.append(query->value(query->record().indexOf("id")).toInt());
            actual_rec_region_list.append(query->value(query->record().indexOf("region_id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getRegionDeliveriesByDate: id,region_id query error - "
                << query->lastError();

        return cMinusTwo;
    }


    query->prepare("SELECT id FROM regions");

    if(query->exec())
    {
        while (query->next())
        {
            region_id_list.append(query->value(query->record().indexOf("id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getRegionDeliveriesByDate: id query error - "
                << query->lastError();

        return cMinusTwo;
    }

    // get actual recipient deliveries
    for(int i = 0; i < actual_rec_id_list.size(); i++)
    {
        temp_total = 0;

        for(int j = 0; j < rec_id_list.size(); j++)
        {
            if(actual_rec_id_list.at(i) == rec_id_list.at(j))
            {
                temp_total += del_list.at(j);
            }
        }

        actual_rec_del_count_list.append(temp_total);
    }

    // get actual region deliveries
    for(int j = 0; j < region_id_list.size(); j++)
    {
        temp_total = 0;

        for(int i = 0; i < actual_rec_region_list.size(); i++)
        {
            if(actual_rec_region_list.at(i) == region_id_list.at(j))
            {
                temp_total += actual_rec_del_count_list.at(i);
            }
        }

        region_delivery.id    = region_id_list.at(j);
        region_delivery.count = temp_total;

        temp_string.clear();
        getRegionNameById(temp_string,region_id_list.at(j));
        region_delivery.region = temp_string;

        temp_string.clear();
        getDistricNameById(temp_string,region_id_list.at(j));
        region_delivery.district = temp_string;

        region_delivery_list.append(region_delivery);
    }

    return cZero;
}

/*!
 * \brief getSchoolDeliveriesByDate
 * \param from
 * \param to
 * \param reg_del_list
 * \return 0 records ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getSchoolDeliveriesByDate( QDate                    from,
                                         QDate                    to,
                                         QList<SchoolDeliveries>& school_delivery_list)

{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // temp variables
    int temp_total;
    QString temp_string;

    SchoolDeliveries school_delivery;

    QList<int> rec_id_list;
    QList<int> del_list;
    QList<int> actual_rec_id_list;
    QList<int> school_id_list;
    QList<int> actual_rec_school_list;
    QList<int> actual_rec_del_count_list;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getSchoolDeliveriesByDate: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT recipient_id, packs_count FROM distribution_activity "
                   "WHERE date >= (:from) AND date <= (:to) ");

    query->bindValue(":from",from);
    query->bindValue(":to"  ,to);

    if(query->exec())
    {
        while (query->next())
        {
            rec_id_list.append(query->value(query->record().indexOf("recipient_id")).toInt());
            del_list.append(query->value(query->record().indexOf("packs_count")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getSchoolDeliveriesByDate: recipient_id, packs_count query error - "
                << query->lastError();

        return cMinusTwo;
    }

    query->prepare("SELECT id,school_id FROM recipients");

    if(query->exec())
    {
        while (query->next())
        {
            actual_rec_id_list.append(query->value(query->record().indexOf("id")).toInt());
            actual_rec_school_list.append(query->value(query->record().indexOf("school_id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getSchoolDeliveriesByDate: id,school_id query error - "
                << query->lastError();

        return cMinusTwo;
    }


    query->prepare("SELECT id FROM schools");

    if(query->exec())
    {
        while (query->next())
        {
            school_id_list.append(query->value(query->record().indexOf("id")).toInt());
        }
    }
    else
    {
        qDebug()<< "Database::getSchoolDeliveriesByDate: id query error - "
                << query->lastError();

        return cMinusTwo;
    }

    // get actual recipient deliveries
    for(int i = 0; i < actual_rec_id_list.size(); i++)
    {
        temp_total = 0;

        for(int j = 0; j < rec_id_list.size(); j++)
        {
            if(actual_rec_id_list.at(i) == rec_id_list.at(j))
            {
                temp_total += del_list.at(j);
            }
        }

        actual_rec_del_count_list.append(temp_total);
    }

    // get actual school deliveries
    for(int j = 0; j < school_id_list.size(); j++)
    {
        temp_total = 0;

        for(int i = 0; i < actual_rec_school_list.size(); i++)
        {
            if(actual_rec_school_list.at(i) == school_id_list.at(j))
            {
                temp_total += actual_rec_del_count_list.at(i);
            }
        }

        school_delivery.id    = school_id_list.at(j);
        school_delivery.count = temp_total;

        temp_string.clear();
        getSchoolNameById(temp_string,school_id_list.at(j));
        school_delivery.school = temp_string;

        school_delivery_list.append(school_delivery);
    }

    return cZero;
}


/*!
 * \brief getNumberOfRecipientsIssuedToByMonth
 * \param from
 * \param to
 * \param dist_activity_list
 * \return 0 records ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getNumberOfRecipientsIssuedToByMonth( int month_num,
                                                    int& count)

{
    // const int variables
    const int cOne          = 1;
    const int cTwelve       = 12;
    const int cZero         = 0;
    const int cMinusOne     = -1; // db conn false
    const int cMinusTwo     = -2; // query error

    count = 0;

    // start date
    QDate start_date(QDate::currentDate().year(), month_num, cOne);
    // end date
    QDate end_date;
    if(month_num < cTwelve)
        end_date = QDate(QDate::currentDate().year(), month_num + cOne, cOne);
    else
        end_date = QDate(QDate::currentDate().year(), cOne, cOne);

    //qDebug()<<"start - " << start_date.toString()<<" end- "<<end_date.toString();

    // temp variables
    int temp_total;

    QList<int> rec_id_list;
    QList<int> del_list;

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getNumberOfRecipientsIssuedToByMonth: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT DISTINCT recipient_id FROM distribution_activity "
                   "WHERE date >= (:from) AND date <= (:to) ");

    query->bindValue(":from",start_date);
    query->bindValue(":to"  ,end_date);

    temp_total = 0;
    if(query->exec())
    {
        while (query->next())
        {
            rec_id_list.append(query->value(query->record().indexOf("recipient_id")).toInt());
            del_list.append(query->value(query->record().indexOf("packs_count")).toInt());

            temp_total++;
        }
        count = temp_total;

    }
    else
    {
        qDebug()<< "Database::getNumberOfRecipientsIssuedToByMonth: recipient_id, packs_count query error - "
                << query->lastError();

        return cMinusTwo;
    }
    return cZero;
}


/*!
 * \brief getDistributionActivitySummaryByDate
 * \param from
 * \param to
 * \param dist_activity_list
 * \return 0 records ids retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getDistributionActivitySummaryByDate( QDate                         from,
                                                    QDate                         to,
                                                    int                           recipient_id,
                                                    DistributionActivitySummary&  dist_activity)
{

    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    dist_activity.clear();

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getDistributionActivitySummaryByDate: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT COUNT(accepted_by) FROM distribution_activity WHERE accepted_by = (:by) AND recipient_id = (:rec_id) "
                   "AND date >= (:from) AND date <= (:to) GROUP BY accepted_by");

    query->bindValue(":by"      , getPackReceiverToString(SELF));
    query->bindValue(":rec_id"  ,recipient_id);
    query->bindValue(":from"    ,from);
    query->bindValue(":to"      ,to);

    if(query->exec())
        while (query->next())
            dist_activity.packs_rcvd_by_self_count  = query->value(0).toInt();
    else
        qDebug()<< "Database::getDistributionActivitySummaryByDate: accepted_by = 'SELF' query error - "
                << query->lastError();

    query->finish();

    query->prepare("SELECT COUNT(accepted_by) FROM distribution_activity WHERE accepted_by = (:by) AND recipient_id = (:rec_id) "
                   "AND date >= (:from) AND date <= (:to) GROUP BY accepted_by");

    query->bindValue(":by", getPackReceiverToString(AUTHORIZED));
    query->bindValue(":rec_id",recipient_id);
    query->bindValue(":from"  ,from);
    query->bindValue(":to"    ,to);

    if(query->exec())
        while (query->next())
            dist_activity.packs_rcvd_by_authorised_count  = query->value(0).toInt();
    else
        qDebug()<< "Database::getDistributionActivitySummaryByDate: accepted_by = AUTHORIZED query error - "
                << query->lastError();

    query->clear();

    query->prepare("SELECT COUNT(accepted_by) FROM distribution_activity WHERE accepted_by = (:by) AND recipient_id = (:rec_id) "
                   "AND date >= (:from) AND date <= (:to) GROUP BY accepted_by");

    query->bindValue(":by", getPackReceiverToString(ISSUER));
    query->bindValue(":rec_id",recipient_id);
    query->bindValue(":from"  ,from);
    query->bindValue(":to"    ,to);

    if(query->exec())
        while (query->next())
            dist_activity.packs_rcvd_by_issuer_count  = query->value(0).toInt();
    else
        qDebug()<< "Database::getDistributionActivitySummaryByDate: accepted_by = 'ISSUER' query error - "
                << query->lastError();

    query->clear();


    query->prepare("SELECT SUM(packs_count) FROM distribution_activity WHERE recipient_id = (:rec_id) "
                   "AND date >= (:from) AND date <= (:to) GROUP BY recipient_id");

    query->bindValue(":rec_id",recipient_id);
    query->bindValue(":from"  ,from);
    query->bindValue(":to"    ,to);

    if(query->exec())
        while (query->next())
            dist_activity.packs_count  = query->value(0).toInt();
    else
        qDebug()<< "Database::getDistributionActivitySummaryByDate: SUM query error - "
                << query->lastError();

    return cZero;
}

/*!
 * \brief getNotCollectedPacksCount
 * \param from
 * \param to
 * \param dist_activity_list
 * \return 0 count retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getNotCollectedPacksCount(int   recipient_id,
                                        int&  count)
{
    // const int variables
    const int cTwelve   = 12;
    const int cOne      = 1;
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error


    // date variables
    QDate today         = QDateTime::currentDateTime().date();
    QDate date;

    // initialise count
    int months_diff     = 0;
    int years_diff      = 0;
    count               = 0;


    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getNotCollectedPacksCount: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT date FROM distribution_activity WHERE recipient_id = (:id) order by date desc");
    query->bindValue(":id",recipient_id);


    if(query->exec())
    {
        while (query->next())
        {
            date        = query->value(query->record().indexOf("date")).toDate();
            break;
        }

        count         = cZero;

        if(date.month() > cZero)
        {
            months_diff = today.month() - date.month();
            years_diff  = today.year()  - date.year();

            if(years_diff == cZero)
                count     = (months_diff  <= cZero ? cZero : months_diff);
            else if(years_diff > cZero)
                count     = today.month() + (cTwelve - date.month()) +
                        (years_diff > cOne ? (cTwelve * (years_diff - cOne)) : cZero);
        }

    }
    else
    {
        qDebug()<< "Database::getNotCollectedPacksCount: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}


/*!
 * \brief getLastRecieptDateById
 * \param recipient_id
 * \param last_date
 * \return 0 last date retrived successfully
 *         -1 db conn false
 *         -2 query error - details
 */
int Database::getLastRecieptDateById( int    recipient_id,
                                      QDate& last_date)
{
    // const int variables
    const int cZero     = 0;  // results found
    const int cMinusOne = -1; // db conn false
    const int cMinusTwo = -2; // query error

    // check status of db connection
    if(!db_conn_state_){
        qDebug()<< "Database::getLastRecieptDateById: db_conn_state false";
        return cMinusOne;
    }

    // get user from db from db
    QSqlQuery* query = new QSqlQuery(db_handler_);

    query->prepare("SELECT date FROM distribution_activity WHERE recipient_id = (:recipient_id) order by date desc");
    query->bindValue(":recipient_id",recipient_id);

    if(query->exec())
    {
        while (query->next())
        {
            last_date = query->value(query->record().indexOf("date")).toDate();
            break;
        }
    }
    else
    {
        qDebug()<< "Database::getLastRecieptDateById: query error - "
                << query->lastError();

        return cMinusTwo;
    }

    return cZero;
}










#ifndef IDENTIFICATION_SCREEN_H
#define IDENTIFICATION_SCREEN_H

#include <QDialog>
#include <system_user.h>
#include <SystemShare.h>
#include <fingerprintscanner.h>
#include <database_controller.h>

namespace Ui {
class IdentificationScreen;
}

class IdentificationScreen : public QDialog
{
  Q_OBJECT

public:
  explicit IdentificationScreen(QWidget *parent = 0);
  ~IdentificationScreen();

  void clearScreen();
  void setVisibleAuthReceivedStats(bool visibility);
  void populateRecipientsOnAuthScreen();

signals:

private slots:


  void on_ls_pb_scan_finger_clicked();

  void on_ls_pb_cancel_clicked();

  void on_is_cmBx_districts_currentIndexChanged(int index);

  void on_is_pb_set_id_range_clicked();

  void on_is_pb_rescan_clicked();

  void on_is_pb_confim_clicked();

  void on_is_pb_cancel_clicked();

  void on_is_pb_rescan_2_clicked();

  void on_is_pb_cancel_2_clicked();

  void on_is_pb_confim_2_clicked();

  void on_is_pb_auth_received_clicked();

  void on_is_lv_auth_recipients_list_clicked(const QModelIndex &index);

  void on_is_pb_issuer_clicked();

  void on_is_cmBx_schools_currentTextChanged(const QString &);

private:
  Ui::IdentificationScreen *ui;

  // int
  int                       login_tries;
  // enum
  userType                  type_user;
  // scanner access
  FingerprintScanner*       identification_finger_scanner;
  // all local regions list
  QList<RegionDetails>      regions_list;
  // all schools list
  QList<SchoolDetails>      school_list;
  // all recipients for selected regions
  QList<RecipientsDetails>  recipients_list;
  // temp recipients
  RecipientsDetails         temp_recipient;
  // temp receiver
  packReceiver              temp_reciever;
  // selected options
  bool                      all_districs_selected;
  bool                      all_schools_selected;
  static QString            selected_search_school;
  static RegionDetails      selected_search_region;

};

#endif // IDENTIFICATION_SCREEN_H

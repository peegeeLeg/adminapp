#include "useradministration.h"
#include "ui_useradministration.h"
#include <QDebug>
#include <QThread>
#include <QDir>
/*!
 * \brief UserAdministration::UserAdministration
 * \param parent
 */
UserAdministration::UserAdministration(QWidget *parent) :
  QDialog(parent, Qt::CustomizeWindowHint),
  ui(new Ui::UserAdministration)
{
  // set parent screen
  ui->setupUi(this);

  ui->ua_pb_biometric_capture->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->ua_pb_biometric_capture->setIconSize(ui->ua_pb_biometric_capture->size());

  identification_finger_scanner = new FingerprintScanner;

  add_user_template = NULL;

  // set default values
  new_user = false;
  system_users_list_index = 0;
  system_users_list.clear();

  // Change colour of frame to white
  QPalette pal = this->palette();
  pal.setColor(QPalette::Window, Qt::white);
  this->setPalette(pal);
}

/*!
 * \brief UserAdministration::~UserAdministration
 */
UserAdministration::~UserAdministration()
{
  delete ui;
}

/*!
 * \brief UserAdministration::setAdminTypeScreen
 * \param newUser
 */
void UserAdministration::setAdminTypeScreen(adminAction action)
{

  // QStringlist variables
  QStringList user_type_items;
  QStringList status_items;

  // populate system users
  getAllSystemUsers(system_users_list);

  // set default
  new_user                = false;
  system_users_list_index = 0;
  ui->ua_pb_biometric_capture->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->ua_pb_biometric_capture->setIconSize(ui->ua_pb_biometric_capture->size());

  ui->ua_le_password->clear();
  ui->ua_le_password->setEnabled(false);
  ui->ua_le_username->clear();
  ui->ua_le_username->setEnabled(false);

  // set relevant buttons
  switch (action) {
  case AddUser:
    // set
    new_user                = true;

    currentAdminAction  = AddUser;

    // set widgets
    ui->ua_lbl_fp_capture_status->clear();
    ui->ua_pb_biometric_capture->setEnabled(true);
    ui->ua_pb_cancel->setVisible(true);
    ui->ua_pb_delete->setVisible(false);
    ui->ua_pb_search->setVisible(false);
    ui->ua_pb_updateAndClose->setVisible(true);
    ui->ua_le_password->clear();
    ui->ua_le_password->setEnabled(true);
    ui->ua_le_username->clear();
    ui->ua_le_username->setEnabled(true);
    ui->ua_lbl_title_bar->setText(normalMsg("ADD NEW SYSTEM USER"));
    ui->ua_pb_updateAndClose->setText("Add User");

    break;

  case EditUser:
    // set
    currentAdminAction  = EditUser;

    // set widgets
    ui->ua_pb_cancel->setVisible(true);
    ui->ua_pb_delete->setVisible(false);
    ui->ua_pb_search->setVisible(false);
    ui->ua_lbl_fp_capture_status->clear();
    ui->ua_pb_biometric_capture->setEnabled(true);
    ui->ua_pb_updateAndClose->setVisible(true);
    ui->ua_lbl_title_bar->setText(normalMsg("EDIT SYSTEM USER"));
    ui->ua_pb_updateAndClose->setText("Edit User");
    break;

  case DeleteUser:
    // set
    currentAdminAction  = DeleteUser;
    // set widgets
    ui->ua_lbl_fp_capture_status->clear();
    ui->ua_pb_biometric_capture->setEnabled(false);
    ui->ua_pb_cancel->setVisible(true);
    ui->ua_pb_delete->setVisible(true);
    ui->ua_pb_search->setVisible(false);
    ui->ua_pb_updateAndClose->setVisible(false);
    ui->ua_lbl_title_bar->setText(normalMsg("DELETE SYSTEM USER"));
    break;
  case AuthenticateUser:
    // set
    currentAdminAction  = AuthenticateUser;
    break;
  default:
    break;
  }

  // set status options
  status_items <<getAccStatusToString(Active)
              <<getAccStatusToString(Blocked);

  ui->ua_cmbx_Status->clear();
  ui->ua_cmbx_Status->addItems(status_items);

  // set relevent options
  switch (SystemUser::Instance()->getUserType()) {

  case SuperAdmin:
    //-- set user type options
    user_type_items.clear();
    user_type_items <<getUserTypeToString(Nurse)
                   <<getUserTypeToString(Admin)
                  <<getUserTypeToString(SuperAdmin);

    //- set user type options
    ui->ua_cmbx_user_type->clear();
    ui->ua_cmbx_user_type->addItems(user_type_items);
    break;

  case Admin:
    //-- set user type options
    user_type_items.clear();
    user_type_items <<getUserTypeToString(Nurse)
                   <<getUserTypeToString(Admin);

    //- set user type options
    ui->ua_cmbx_user_type->clear();
    ui->ua_cmbx_user_type->addItems(user_type_items);
    break;

  default:
    user_type_items.clear();
    status_items.clear();
    ui->ua_cmbx_user_type->clear();
    ui->ua_cmbx_Status->clear();
    ui->ua_pb_updateAndClose->setVisible(false);
    break;
  }
}


/*!
 * \brief UserAdministration::AddSystemUser
 * \return true if sys user added successfully
 *         false if sys user failed to be added
 */
bool UserAdministration::addSystemUser()
{
  // result variable
  int result;

  // QString
  QString     username        = "";
  QString     password        = "";
  UserDetails* detailsOfUser  = new UserDetails;

  // collect inputs
  username                            = ui->ua_le_username->text();
  password                            = ui->ua_le_password->text();
  detailsOfUser->name                 = ui->ua_le_name->text();
  detailsOfUser->surname              = ui->ua_le_surname->text();
  detailsOfUser->email_address        = ui->ua_le_email->text();
  detailsOfUser->contact_number       = ui->ua_le_contactNumber->text();
  detailsOfUser->user_type            = getStringToUserType(ui->ua_cmbx_user_type->currentText());
  detailsOfUser->account_status       = getStringToAccStatus(ui->ua_cmbx_Status->currentText());
  detailsOfUser->fingerprint_template = add_user_template;
  detailsOfUser->fingerprint_enrolled = (add_user_template == NULL? false:true);


  // validate inputs
  if(username                                 == "" ||
     password                                 == "" ||
     detailsOfUser->name.trimmed()            == "" ||
     detailsOfUser->surname.trimmed()         == "" ||
     detailsOfUser->email_address.trimmed()   == "" ||
     detailsOfUser->contact_number.trimmed()  == ""){

    //- display no error
    ui->ua_lbl_info_error->setText(errorMsg("Enter all required fields!"));

    return false;
  }

  // edit sys user
  result = DatabaseController::active_db_handle_->addSystemUser(username,
                                                                password,
                                                                detailsOfUser);

  // assess result
  if(result == 0){

    //- display message
    ui->ua_lbl_info_error->setText(successMsg("User added succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Added new system user called %1 , with usertype: %2 %3")
                                                 .arg(detailsOfUser->name + " " + detailsOfUser->surname)
                                                 .arg(getUserTypeToString(detailsOfUser->user_type))
                                                 .arg((detailsOfUser->fingerprint_enrolled == true ?
                                                         ", And enrolled a fingerprint":
                                                         ", But did not enroll fingerprint")));


    return true;
  }
  else if (result == -1){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Enter All required fields!"));
  }
  else if (result == -3){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Please enroll recipients fingerprint"));
  }
  else if (result == -4){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;

}


/*!
 * \brief UserAdministration::editSystemUser
 * \return true if sys user editted successfully
 *         false if sys user failed to be editted
 */
bool UserAdministration::editSystemUser()
{
  // result variable
  int result;

  // User details
  UserDetails* detailsOfUser  = new UserDetails;

  // collect inputs
  detailsOfUser->name                 = ui->ua_le_name->text();
  detailsOfUser->surname              = ui->ua_le_surname->text();
  detailsOfUser->email_address        = ui->ua_le_email->text();
  detailsOfUser->contact_number       = ui->ua_le_contactNumber->text();

  detailsOfUser->user_type            = getStringToUserType(ui->ua_cmbx_user_type->currentText());
  detailsOfUser->account_status       = getStringToAccStatus(ui->ua_cmbx_Status->currentText());

  detailsOfUser->fingerprint_template = add_user_template;
  detailsOfUser->fingerprint_enrolled = (add_user_template == NULL? SystemUser::current_recipient_details.fingerprint_enrolled:true);

  // Validate inputs
  if(system_users_list_index <= 0){

    //- display no error
    ui->ua_lbl_info_error->setText(errorMsg("Select System User!"));

    return false;
  }
  if(detailsOfUser->name.trimmed()            == "" ||
     detailsOfUser->surname.trimmed()         == "" ||
     detailsOfUser->email_address.trimmed()   == "" ||
     detailsOfUser->contact_number.trimmed()  == ""){

    //- display no error
    ui->ua_lbl_info_error->setText(errorMsg("Enter all required fields!"));

    return false;
  }


  // add sys user
  result = DatabaseController::active_db_handle_->editSystemUser( detailsOfUser,
                                                                  system_users_list_index);

  // assess result
  if(result == 0){

    //- display no error
    ui->ua_lbl_info_error->setText(successMsg("User editted succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Editted system user called %1 , with usertype: %2")
                                                 .arg(detailsOfUser->name + " " + detailsOfUser->surname)
                                                 .arg(getUserTypeToString(detailsOfUser->user_type)));

    return true;
  }
  else if (result == -1){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Enter both Name and Surname"));
  }
  else if (result == -3){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;

}


/*!
 * \brief UserAdministration::deleteSystemUser
 * \return true if sys user deleted successfully
 *         false if sys user failed to be deleted
 */
bool UserAdministration::deleteSystemUser()
{
  // result variable
  int result;

  // Validate inputs
  if(system_users_list_index <= 0){

    //- display no error
    ui->ua_lbl_info_error->setText(errorMsg("Select System User!"));

    return false;
  }

  //get details for logging first
  UserDetails detailsOfUser;
  DatabaseController::active_db_handle_->getUserDetailsById(detailsOfUser,system_users_list_index);


  // delete sys user
  result = DatabaseController::active_db_handle_->deleteSystemUser(system_users_list_index);

  // assess result
  if(result == 0){

    //- display no error

    // Change colour of frame to white
    ui->ua_lbl_info_error->setText(successMsg("User deleted succefully!"));

    DatabaseController::active_db_handle_->trail(QString("Deleted system user called %1 , with usertype: %2")
                                                 .arg(detailsOfUser.name + " " + detailsOfUser.surname)
                                                 .arg(getUserTypeToString(detailsOfUser.user_type)));

    return true;
  }
  else if (result == -1){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }

  // return
  return false;
}

/*!
 * \brief UserAdministration::getAllSystemUsers
 * \param users_list
 * \param detailsOfUser
 * \return true if users list was retrived successfully
 *         false if users list failed to be retrived
 */
void UserAdministration::getAllSystemUsers(QList< QPair<UserDetails, int> >& users_list)
{
  users_list.clear();
  // result variable
  int result;

  // add sys user
  users_list.clear();
  result = DatabaseController::active_db_handle_->getAllSystemUsers(users_list);

  // assess result
  if(result == 0){

    //- Populate the actual view
    ui->ua_list_system_users->clear();

    for(int i = 0; i < system_users_list.size(); i++)
    {
      ui->ua_list_system_users->addItem(getUserTypeToString(system_users_list.at(i).first.user_type) +
                                        " - "                                                       +
                                        system_users_list.at(i).first.name                         +
                                        " "                                                         +
                                        system_users_list.at(i).first.surname);

    }


  }
  else if (result == -1){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Enter both Username & Password"));
  }
  else if (result == -3){
    //- display error
    ui->ua_lbl_info_error->setText(errorMsg("Unknown Add System User error. Contact Service Provider"));
  }
}

/*!
 * \brief UserAdministration::on_ua_pb_search_clicked
 */
void UserAdministration::on_ua_pb_search_clicked()
{
  // Only if existing user (Edit Mode)

  // search from Name and/or surname
  QString name    = ui->ua_le_name->text();
  QString surname = ui->ua_le_surname->text();

  // Search
}


/*!
 * \brief UserAdministration::on_ua_pb_updateAndClose_clicked
 */
void UserAdministration::on_ua_pb_updateAndClose_clicked()
{
  // variable
  bool update_success = false;
  QString success_msg = "";

  if(currentAdminAction == AddUser)
  {
    // add record
    update_success = addSystemUser();
    success_msg    = "System User Added Successfully!";
  }
  else if(currentAdminAction == EditUser)
  {
    // update record
    update_success = editSystemUser();
    success_msg    = "System User Editted Successfully!";
  }

  // assess result
  if(update_success){
    //- clear screen if all went well
    this->clearScreen();

    //- update sys user list
    getAllSystemUsers(system_users_list);

    //- display essage
    ui->ua_lbl_info_error->setText(successMsg(success_msg));

  }
}


/*!
 * \brief UserAdministration::on_ua_pb_delete_clicked
 */
void UserAdministration::on_ua_pb_delete_clicked()
{
  // integers
  bool delete_success = false;
  QString success_msg = "";

  if(currentAdminAction == DeleteUser)
  {
    // add record
    delete_success = deleteSystemUser();
    success_msg    = "System User Deleted Successfully!";
  }


  // assess result
  if(delete_success){
    //- clear screen if all went well
    this->clearScreen();

    //- update sys user list
    getAllSystemUsers(system_users_list);

    //- display essage
    ui->ua_lbl_info_error->setText(successMsg(success_msg));
  }
}

/*!
 * \brief UserAdministration::on_ua_pb_cancel_clicked
 */
void UserAdministration::on_ua_pb_cancel_clicked()
{
  // clear screen
  this->clearScreen();
  // close without saving
  this->close();
}

/*!
 * \brief UserAdministration::clearScreen
 */
void UserAdministration::clearScreen()
{
  // clear contents
  ui->ua_le_name->clear();
  ui->ua_le_email->clear();
  ui->ua_le_surname->clear();
  ui->ua_le_password->clear();
  ui->ua_le_username->clear();
  ui->ua_le_contactNumber->clear();
  ui->ua_lbl_info_error->clear();
  system_users_list_index = 0;
  system_users_list.clear();

  add_user_template  = NULL;
}

/*!
 * \brief UserAdministration::on_ua_list_system_users_clicked
 * \param index
 */
void UserAdministration::on_ua_list_system_users_clicked(const QModelIndex &index)
{
  // int
  int ind = index.row();

  // check if new user is beign added
  if(new_user)
  {
    ui->ua_lbl_info_error->setText(warningMsg("New user being entered"));
    return;
  }

  ui->ua_le_name->setText(system_users_list.at(ind).first.name);
  ui->ua_le_email->setText(system_users_list.at(ind).first.email_address);
  ui->ua_le_surname->setText(system_users_list.at(ind).first.surname);
  ui->ua_le_contactNumber->setText(system_users_list.at(ind).first.contact_number);
  ui->ua_cmbx_Status->setCurrentText(getAccStatusToString(system_users_list.at(ind).first.account_status));
  ui->ua_cmbx_user_type->setCurrentText(getUserTypeToString(system_users_list.at(ind).first.user_type));


  system_users_list_index = system_users_list.at(ind).second;

}

void UserAdministration::on_ua_pb_biometric_capture_clicked()
{
  ui->ua_pb_biometric_capture->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->ua_pb_biometric_capture->setIconSize(ui->ua_pb_biometric_capture->size());

  add_user_template.clear();
  template_quality = 0;

  identification_finger_scanner->capture(add_user_template, template_quality);

  ui->ua_pb_biometric_capture->setIcon(QIcon("just_captured.bmp"));
  ui->ua_pb_biometric_capture->setIconSize(ui->ua_pb_biometric_capture->size());

  QDir dir;
  dir.remove("just_captured.bmp");

  if(template_quality <= MIN_FP_QUALITY)
  {
    // must clear captured image
    add_user_template.clear();
    ui->ua_lbl_info_error->setText(errorMsg(QString("Fingerprint Quality too low %1. Expecting more than %2!")
                                            .arg(QString::number(template_quality))
                                            .arg(QString::number(MIN_FP_QUALITY))));
    ui->ua_lbl_fp_capture_status->setText(errorMsg("POOR Quality!"));
    return;
  }
  else
  {
    ui->ua_lbl_fp_capture_status->setText(successMsg("Good Quality!"));
  }

  // check fingerprint for duplication
  // get all ids of the stored template
  QList<int>            template_ids_list;
  QPair<QByteArray,int> stored_template;
  bool                  verify_success  = false;
  int                   matched_id      = 0;

  template_ids_list.clear();
  DatabaseController::active_db_handle_->getAllTemplateIds(template_ids_list,
                                                           generalUser);
  // assess against stored
  for(int i = 0; i < template_ids_list.size(); i++)
  {
    //- clear container
    stored_template.first.clear();

    //- get next stored template
    DatabaseController::active_db_handle_->getTemplateById(template_ids_list.at(i),
                                                           stored_template,
                                                           generalUser);

    // verify
    if(identification_finger_scanner->verify(add_user_template,stored_template.first))
    {
      verify_success  = true;
      matched_id      = stored_template.second;
      break;
    }
  }
  if(verify_success)
  {
    UserDetails user;
    DatabaseController::active_db_handle_->getUserDetailsById(user,matched_id);
    //- display no error
    ui->ua_lbl_info_error->setText(errorMsg(QString("Fingerprint already enrolled for - %1 %2!")
                                            .arg(user.name)
                                            .arg(user.surname)));

    add_user_template.clear();
  }
  else
  {
    ui->ua_lbl_info_error->setText(successMsg("Fingerprint Captured!"));
  }

}

























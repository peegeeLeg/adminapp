#-------------------------------------------------
#
# Project created by QtCreator 2018-08-14T18:46:18
#
#-------------------------------------------------

QT += core gui sql printsupport
QT += core network sql
QT += axcontainer
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sebenza
TEMPLATE = app

 INCLUDEPATH += "biomini"

SOURCES += main.cpp\
        homescreen.cpp \
    login_screen.cpp \
    database_controller.cpp \
    database.cpp \
    useradministration.cpp \
    system_user.cpp \
    SystemShare.cpp \
    fingerprintscanner.cpp \
    simplecrypt.cpp \
    identification_screen.cpp

HEADERS  += homescreen.h \
    login_screen.h \
    SystemShare.h \
    constants.h \
    database_controller.h \
    database.h \
    useradministration.h \
    system_user.h \
    fingerprintscanner.h \
    simplecrypt.h \
    biomini/UFDatabase.h \
    biomini/UFExtractor.h \
    biomini/UFMatcher.h \
    biomini/UFScanner.h \
    identification_screen.h

FORMS    += homescreen.ui \
    login_screen.ui \
    useradministration.ui \
    fingerprintscanner.ui \
    identification_screen.ui

RESOURCES += \
    Kumaka_Resource.qrc
  # - Libraries
  LIBS += "UFDatabase.dll"\
          "UFExtractor.dll"\
          "UFMatcher.dll"\
          "UFScanner.dll"

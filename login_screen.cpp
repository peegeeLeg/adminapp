#include "login_screen.h"
#include "ui_login_screen.h"
#include <fingerprintscanner.h>
#include <QMessageBox>
#include <QMovie>

Login_Screen::Login_Screen(QWidget *parent) :
  QDialog(parent, Qt::CustomizeWindowHint),
  ui(new Ui::Login_Screen)
{
  // set screen and parent
  ui->setupUi(this);
  this->setWindowFlags(Qt::WindowStaysOnTopHint);

  // initialise user
  type_user = generalUser;

  // initialise variables
  login_tries = 0;

  // setup Screen
  ui->ls_lbl_icon_info->setPixmap(QPixmap::fromImage(QImage((":/Company/Logo/kumaka logo.png"))));
  ui->ls_pb_scan_finger->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->ls_pb_scan_finger->setIconSize(ui->ls_pb_scan_finger->size());

  ui->ls_pb_forgot_password->setVisible(false);

  /// indicate db access status to ui TODO


  // Change colour of frame to white
  QPalette pal = this->palette();
  pal.setColor(QPalette::Window, Qt::white);
  this->setPalette(pal);

  // add loading scene
  QMovie *movie = new QMovie(":/Company/Logo/loading.gif");
  ui->ls_lbl_info_diplsyProgress->setMovie(movie);
  movie->start();

  login_finger_scanner = new FingerprintScanner();

  if(login_finger_scanner->initializeScanner())
  {
    ui->ls_lbl_info_main->setText(successMsg("Press button below..."));

    // change to green tick
    ui->ls_lbl_info_diplsyProgress->setPixmap(QPixmap::fromImage(QImage(":/Company/Logo/greenTick2.jpg")));
  }

  // hide forgot password
  ui->ls_frame_forgot_password->setVisible(false);
}

Login_Screen::~Login_Screen()
{
  delete ui;
}

void Login_Screen::on_ls_pb_login_clicked()
{
  // initialise
  int kThree        = 3;
  QString user_name = "";
  QString password  = "";

  // get login details from screen
  user_name = ui->ls_le_username->text();
  password  = ui->ls_le_password->text();

  // verify details
  bool login_success = VerifyCredentials(user_name, password);

  // check
  if(login_success)
  {
    // - emit success login
    emit signal_login(true, type_user);

    // - close window
    this->close();
  }
  else if(login_tries >= kThree)
  {
    // - emit failed login
    emit signal_login(false,type_user);

    // - close window
    QApplication::quit();
    this->close();
  }
}

/*!
 * \brief Login_Screen::VerifyCredentials
 * \param Username
 * \param Password
 * \return true if access granted
 *         false if access denied
 */
bool Login_Screen::VerifyCredentials(QString Username, QString Password)
{
  // result
  int result;

  // expected values on successful verification
  int       id              = 0;
  accStatus acc_status;
  userType  user_type;
  QString   name            = "";
  QString   surname         = "";
  QString   email           = "";
  QString   contact_number  = "";

  // check credentials in db
  result = DatabaseController::active_db_handle_->verifyCredentials(Username,
                                                                    Password,
                                                                    user_type,
                                                                    id,
                                                                    name,
                                                                    surname,
                                                                    email,
                                                                    contact_number,
                                                                    acc_status);
  // assess result
  if(result == 0){
    SystemUser::Instance()->setId(id);
    SystemUser::Instance()->setName(name);
    SystemUser::Instance()->setEmail(email);
    SystemUser::Instance()->setSurname(surname);
    SystemUser::Instance()->setLoginStatus(true);
    SystemUser::Instance()->setUserType(user_type);
    SystemUser::Instance()->setContactNumber(contact_number);

    DatabaseController::active_db_handle_->trail("Successfully logged in the system using credentials");

    return true;
  }
  else if (result == -1){
    //- display error
    ui->ls_lbl_error_msg->setText(errorMsg("No database connection"));
  }
  else if (result == -2){
    //- display error
    ui->ls_lbl_error_msg->setText(errorMsg("Enter both Username & Password"));
  }
  else if (result == -3){
    //- display error
    ui->ls_lbl_error_msg->setText(errorMsg("Username/Password is wrong"));
  }
  else if (result == -4){
    //- display error
    ui->ls_lbl_error_msg->setText(errorMsg("Your account is blocked. Contact Service Provider"));
  }
  else if (result == -5){
    //- display error
    ui->ls_lbl_error_msg->setText(errorMsg("System error. Contact Service Provider"));
  }
  else{
    //- display error
    ui->ls_lbl_error_msg->setText(errorMsg("Unknown Login error. Contact Service Provider"));
  }

  // increment
  login_tries++;

  // return
  return false;

}

void Login_Screen::on_ls_pb_forgot_password_clicked()
{
  // hide cuurent screen
  ui->ls_frame_login->setVisible(false);
  // set screen to forgot password
  ui->ls_frame_forgot_password->setVisible(true);
  // set cursor
  ui->ls_le_email_address->setFocus();
}

void Login_Screen::on_ls_pb_send_email_forgot_password_clicked()
{
    // get email address
  QString email_address = ui->ls_le_email_address->text();

  // send email

  // close window
  this->close();
}

/*!
 * \brief Login_Screen::on_ls_pb_scan_finger_clicked
 */
void Login_Screen::on_ls_pb_scan_finger_clicked()
{
  QByteArray            probe_template;
  QPair<QByteArray,int> stored_template;
  QList<int>            template_ids_list;

  bool                  verify_success = false;
  int                   matched_id;
  int                   quality;

  // capture probe finger
  login_finger_scanner->capture(probe_template,quality);

  // get all ids of the stored template
  DatabaseController::active_db_handle_->getAllTemplateIds(template_ids_list,
                                                           generalUser);
  if(template_ids_list.isEmpty())
  {
    ui->ls_lbl_error_msg->setText(warningMsg("No recipients fingerprints stored!"));
  }

  // assess against stored
  for(int i = 0; i < template_ids_list.size(); i++)
  {
    //- clear container
    stored_template.first.clear();

    //- get next stored template
    DatabaseController::active_db_handle_->getTemplateById(template_ids_list.at(i),
                                                           stored_template,
                                                           generalUser);

    // verify
    if(login_finger_scanner->verify(probe_template,stored_template.first))
    {
      verify_success  = true;
      matched_id      = stored_template.second;
      qDebug()<<"Match found! id - "<<matched_id;
      break;
    }
  }

  // update results
  if(verify_success)
  {
    UserDetails user_details;
    DatabaseController::active_db_handle_->getUserDetailsById(user_details,
                                                              matched_id);
    SystemUser::Instance()->setId(user_details.id);
    SystemUser::Instance()->setName(user_details.name);
    SystemUser::Instance()->setEmail(user_details.email_address);
    SystemUser::Instance()->setSurname(user_details.surname);
    SystemUser::Instance()->setLoginStatus(true);
    SystemUser::Instance()->setUserType(user_details.user_type);
    SystemUser::Instance()->setContactNumber(user_details.contact_number);

    type_user = user_details.user_type;

    DatabaseController::active_db_handle_->trail("Successfully logged in the system using fingerprint");

    // - emit success login
    emit signal_login(true, type_user);

    // - close window
    this->close();

  }
  else
  {
    ui->ls_lbl_error_msg->setText(errorMsg("No match found!"));
    // increment
    login_tries++;

    if(login_tries >= 3)
    {
      // - emit failed login
      emit signal_login(false,type_user);

      // - close window
      QApplication::quit();
      this->close();
    }
  }




}



void Login_Screen::on_ls_pb_cancel_login_clicked()
{
  if(login_finger_scanner->isCapturing())
    login_finger_scanner->abortCapturing();

    this->close();
}

#ifndef DATABASE_H
#define DATABASE_H

// String libraries
#include <QPair>
#include <QString>
#include <QStringList>

//Qt Sql DB libraries
#include <QSqlDriver>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QtSql>

// Other includes
#include <simplecrypt.h>
#include "constants.h"
#include "SystemShare.h"
#include "system_user.h"

class Database
{
public:

  /*!
   * \brief Database
   * \param dbSystem
   * \param dbHost
   * \param dbName
   * \param dbUsername
   * \param dbPassword
   */
  Database(dbSystem      dbSystemType,
           QString       dbHost,
           QString       dbName,
           QString       dbUsername,
           QString       dbPassword);


  ~Database();

  /*!
   * \brief initialiseDb
   * \return -1 if failed to initialize database
   *          0 if successfully initialized database
   */
  int initialiseDb();

  /*!
   * \brief Database::VerifyCredentials
   * \param userName
   * \param passWord
   * \param typeOfUser
   * \param name
   * \param surname
   * \param email
   * \param contact_number
   * \param statusOfAcc
   * \return 0 match found
   *         -1 db conn false
   *         -2 username or password not entered
   *         -3 wrong Username/Password
   *         -4 user account blocked
   *         -5 query error
   */
  int verifyCredentials(QString    userName,
                        QString    passWord,
                        userType&  typeOfUser,
                        int&       id,
                        QString&   name,
                        QString&   surname,
                        QString&   email,
                        QString&   contact_number,
                        accStatus& statusOfAcc);

  /*!
   * \brief Database::addSystemUser
   * \param userName
   * \param passWord
   * \param detailsOfUser*
   * \return 0 system user added successfully
   *         -1 db conn false
   *         -2 username or password not entered
   *         -3 user's Name or Surname not entered
   *         -4 query error
   */
  int addSystemUser(QString      userName,
                    QString      passWord,
                    UserDetails* detailsOfUser);

  /*!
   * \brief Database::addRecipient
   * \param RecipientsDetails*
   * \return 0 system user added successfully
   *         -1 db conn false
   *         -2 Name or Surname not entered
   *         -3 Fingerprint not enrolled
   *         -4 query error
   */
  int addRecipient(RecipientsDetails* detailsOfUser);

  /*!
   * \brief Database::addstaff
   * \param RecipientsDetails*
   * \return 0 staff added successfully
   *         -1 db conn false
   *         -2 Username not entered
   *         -3 Fingerprint not enrolled
   *         -4 query error
   */
  int addStaff(StaffDetails* detailsOfstaff);

  /*!
   * \brief Database::addRegion
   * \param RegionDetails*
   * \return 0 region added successfully
   *         -1 db conn false
   *         -2 District not entered
   *         -3 query error
   */
  int addRegion(RegionDetails* detailsOfRegion);

  /*!
   * \brief Database::addSchool
   * \param SchoolDetails*
   * \return 0 school added successfully
   *         -1 db conn false
   *         -2 School not entered
   *         -3 query error
   */
  int addSchool(SchoolDetails* detailsOfSchool);


  /*!
   * \brief addStockItems
   * \param description
   * \param count
   * \return 0 items added successfully
   *         -1 db conn false
   *         -2 Items not entered
   *         -3 query error
   */
  int addStockItems(QString description,
                    double  count);
  /*!
   * \brief getDignityPackCounts
   * \param count
   * \param action
   * \return 0 packs retrieved successfully
   *         -1 db conn false
   *         -2 count not retrieved
   *         -3 query error
   */
  int getDignityPackCounts(double& count);

  /*!
   * \brief getDignityPackLimitDetails
   * \param count
   * \param action
   * \return 0 packs limit count and email retreived successfully
   *         -1 db conn false
   *         -2 limit/email not retrieved
   *         -3 query error
   */
  int getDignityPackLimitDetails(double&  limit,
                                 QString& email);

  /*!
   * \brief updateDignityPacks
   * \param count
   * \param action
   * \return 0 packs updated successfully
   *         -1 db conn false
   *         -2 Items not entered
   *         -3 query error
   */
  int updateDignityPacks(double             count,
                         QString            comment,
                         dignityPackAction  action);

  /*!
   * \brief updateStockSettings
   * \param limit
   * \param email
   * \return 0 stock settings updated successfully
   *         -1 db conn false
   *         -2 Items not entered
   *         -3 query error
   */
  int updateStockSettings(double  limit,
                                    QString email);
  /*!
   * \brief Database::editSystemUser
   * \param detailsOfUser*
   * \param userIndex
   * \return 0 system user editted successfully
   *         -1 db conn false
   *         -2 user's Name or Surname not entered
   *         -3 query error
   */

  int editSystemUser( UserDetails*  detailsOfUser,
                      int           userIndex);

  /*!
   * \brief Database::deleteSystemUser
   * \param recipientIndex
   * \return 0 recipient deleted successfully
   *         -1 db conn false
   *         -2 query error - details
   *         -3 query error - template
   */
  int deleteSystemUser(int userIndex);

  /*!
   * \brief Database::editRecipient
   * \param RecipientsDetails*
   * \param recipientIndex
   * \return 0 recipient editted successfully
   *         -1 db conn false
   *         -2 user's Name or Surname not entered
   *         -3 fingerprint not enrolled
   *         -4 query error -  details
   *         -5 query error -  fingerprint
   */
  int editRecipient( RecipientsDetails* detailsOfRecipient,
                               int                recipientIndex);



  /*!
   * \brief Database::deleteRecipient
   * \param recipientIndex
   * \return 0 recipient deleted successfully
   *         -1 db conn false
   *         -2 query error - details
   *         -3 query error - template
   */
  int deleteRecipient(int recipientIndex);

  /*!
   * \brief getAllSystemUsers
   * \param users_list
   * \return
   */
  int getAllSystemUsers(QList<QPair<UserDetails, int> >& users_list);

  /*!
   * \brief getAllRecipients
   * \param recipient_list
   * \return
   */
  int getAllRecipients(QList<RecipientsDetails>& recipient_list);

  /*!
   * \brief getRecipientById
   * \param recipient
   * \return
   */
  int getRecipientById(int id, RecipientsDetails &recipient);

  /*!
   * \brief getAllRecipientsTemplates
   * \param templates
   * \return
   */
  int getAllRecipientsTemplates(QList<QPair<QByteArray, int> >& templates);

  /*!
   * \brief Database::getAllRecipientsRegions
   * \param regions_list
   * \return 0 regions retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   **/
  int getAllRecipientsRegions(QList<RegionDetails> &regions_list);

  /*!
   * \brief Database::getSchoolsByDistricId
   * \param regions_list
   * \return 0 schools retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getSchoolsByDistricId(QList< QPair<QString, int> >& schools_list,
                            int                           districtId);


  /*!
   * \brief Database::getSchoolNameById
   * \param schools_list
   * \param id
   * \return 0 school retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getSchoolNameById(QString& schoolsName,
                        int      id);

  /*!
   * \brief getNumberOfSchools
   * \param schools_count
   * \return 0 school count retrived successfully
   *         -1 db conn false
   *         -2 query error
   */
  int getNumberOfSchools(int& schools_count);


  /*!
   * \brief getSchoolDetailsByIdAndRegionId
   * \param school
   * \param school_id
   * \param region_id
   * \return 0 school retrived successfully
   *         -1 db conn false
   *         -2 query error
   */
  int getSchoolDetailsByIdAndRegionId(SchoolDetails&  school,
                                      int             school_id,
                                      int             region_id);
  /*!
   * \brief Database::getSchoolNameById
   * \param schools_list
   * \param id
   * \return 0 school retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getIdBySchoolName(QString schools_list,
                        int&      id);

  /*!
   * \brief Database::getDistricNameById
   * \param schools_list
   * \param id
   * \return 0 school retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getDistricNameById(QString& district_name,
                         int      ind);

  /*!
   * \brief Database::getRegionNameById
   * \param region_name
   * \param id
   * \return 0 region name retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getRegionNameById(QString& region_name,
                        int      ind);

  /*!
   * \brief Database::getIdByDistricName
   * \param schools_list
   * \param id
   * \return 0 school retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getIdByDistricName(QString district_name,
                         int&      id);


  /*!
   * \brief Database::getUserDetailsById
   * \param
   * \param id
   * \return 0 school retrived successfully
   *         -1 db conn false
   *         -2 query error
   **/
  int getUserDetailsById(UserDetails& UserDetails,
                         int      id);
  /*!
   * \brief Database::getAllTemplateIds
   * \param template_ids_list
   * \return 0 templates ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   **/
  int getAllTemplateIds(QList<int>& template_ids_list,
                        userType type_of_user);

  /*!
   * \brief getTemplateById
   * \param id
   * \param stored_template
   * \return 0 templates ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getTemplateById(int                     id,
                      QPair<QByteArray, int>& stored_template,
                      userType type_of_user);

  /*!
   * \brief getRecipientsIdsByRegion
   * \param district_id
   * \param region_recipient_ids_list
   * \return 0 templates ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getRecipientsIdsByRegion(int         district_id,
                               QList<int>& region_recipient_ids_list);

  /*!
   * \brief getRecipientsIdsBySchool
   * \param school_id
   * \param school_recipient_ids_list
   * \return 0 templates ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getRecipientsIdsBySchool(int         school_id,
                               QList<int>& school_recipient_ids_list);

  /*!
   * \brief Database::getTemplateIdsByRecipientId
   * \param recipient_id
   * \param template_ids_list
   * \return 0 templates ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getTemplateIdsByRecipientId(int         recipient_id,
                                  QList<int>& template_ids_list);

  /*!
   * \brief getLastRecieptDateById
   * \param recipient_id
   * \param last_date
   * \return 0 last date retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getLastRecieptDateById(int    recipient_id,
                             QDate& last_date);

  int trail(QString  action);

  /*!
   * \brief log_distribution_activity
   * \param num_of_packs
   * \param received_by
   * \param recipient_id
   * \return 0 log ditribution activity successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int log_distribution_activity(int           num_of_packs,
                                packReceiver  received_by,
                                int           recipient_id);


  /*!
   * \brief getDistributionActivityBy
   * \param from
   * \param to
   * \param dist_activity_list
   * \return 0 records ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getDistributionActivityDetailsByDate( QDate                               from,
                                            QDate                               to,
                                            QList<DistributionActivityDetails> &dist_activity_list);

  /*!
   * \brief getRegionDeliveriesByDate
   * \param from
   * \param to
   * \param reg_del_list
   * \return 0 records ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getRegionDeliveriesByDate( QDate                   from,
                                 QDate                   to,
                                 QList<RegionDeliveries> &reg_del_list);


  /*!
   * \brief getSchoolDeliveriesByDate
   * \param from
   * \param to
   * \param reg_del_list
   * \return 0 records ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getSchoolDeliveriesByDate( QDate                   from,
                                 QDate                   to,
                                 QList<SchoolDeliveries> &school_del_list);

  /*!
   * \brief getDistributionActivitySummaryByDate
   * \param from
   * \param to
   * \param dist_activity_list
   * \return 0 records ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getDistributionActivitySummaryByDate( QDate                       from,
                                            QDate                       to,
                                            int                         recipient_id,
                                            DistributionActivitySummary &dist_activity);

  /*!
   * \brief getNumberOfRecipientsIssuedToByMonth
   * \param from
   * \param to
   * \param dist_activity_list
   * \return 0 records ids retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getNumberOfRecipientsIssuedToByMonth(int  month_num,
                                           int& count);

  /*!
   * \brief getNotCollectedPacksCount
   * \param from
   * \param to
   * \param dist_activity_list
   * \return 0 count retrived successfully
   *         -1 db conn false
   *         -2 query error - details
   */
  int getNotCollectedPacksCount(int   recipient_id,
                                int&  count);


  ///////////////////////////////////////////////////////////////////////////////////

  /*!
   * \brief Database::editStaff
   * \param StaffDetails*
   * \param StaffIndex
   * \return 0 Staff editted successfully
   *         -1 db conn false
   *         -2 user's username not entered
   *         -3 fingerprint not enrolled
   *         -4 query error -  details
   *         -5 query error -  fingerprint
   */
  int editStaff( StaffDetails* detailsOfStaff,
                               int                StaffIndex);



  /*!
   * \brief Database::deleteStaff
   * \param StaffIndex
   * \return 0 Staff deleted successfully
   *         -1 db conn false
   *         -2 query error - details
   *         -3 query error - template
   */
  int deleteStaff(int StaffIndex);

  QString hashSha256(QString plainString){

    return QCryptographicHash::hash(plainString.toLocal8Bit(),
                                    QCryptographicHash::Sha256).toBase64();
  }

  ///////////////////////////////////////////////////////////////////////////////////


//  int get_distribution date
private:

  // db unique settings variables
  QString  db_host_;
  QString  db_name_;
  QString  db_username_;
  QString  db_password_;
  dbSystem db_system_;

  // states
  bool db_conn_state_;
  dbAccessState db_access_state_;

  // db handler
  QSqlDatabase db_handler_;

  // Private methods

  /*!
   * \brief hashSha256
   * \param plainString
   * \return hashedString
   */



  /*!
   * \brief dbSystemString
   * \param db_sys
   * \return db_sys string equivalent if valid
   *         null string if not valid
   */
  QString dbSystemString(dbSystem db_sys){

    if(db_sys == QMYSQL)
      return "QMYSQL";

    else if(db_sys == QSQLITE)
      return "QSQLITE";

    else
      return "INVALID_DB_SYS";

  }


};

#endif // DATABASE_H

#ifndef HOMESCREEN_H
#define HOMESCREEN_H

#include <QMainWindow>
#include <SystemShare.h>
#include <fingerprintscanner.h>
#include <UserAdministration.h>
#include <database_controller.h>


// Screens
#include <login_screen.h>
#include <identification_screen.h>



/*!
 *
 */
namespace Ui {
class HomeScreen;
}

/*!
 * \brief The HomeScreen class
 */
class HomeScreen : public QMainWindow
{
  Q_OBJECT

public:
  explicit HomeScreen(QWidget *parent = 0);
  ~HomeScreen();

  /*!
   * \brief ApplicationLogin - Main Login Screen setup and verify
   * \return
   */
  void ApplicationLogin();

public slots:

  /*!
   * \brief on_login
   * \param loginState
   * \param typeUser
   */
  void on_login(bool loginState, userType typeUser);

private slots:
  void on_actionConfiguration_triggered();

  void on_actionLogout_triggered();


  void on_actionAuthenticate_Recipient_triggered();

  void on_actionNew_triggered();

  void on_actionEdit_triggered();



  void on_hs_tb_authenticate_clicked();

  void on_actionRemove_User_triggered();






private:
  Ui::HomeScreen *ui;

  // login Screen
  Login_Screen*               newLoginScreen;
  // Identify  Recipient
  IdentificationScreen*       newIdentificationScreen;
  // input form
  UserAdministration*         newUserAdmin;
  // fingerprint scanner window
  FingerprintScanner*         newFingerprintScannerWindow;
  // Distribution report date picker screen


  // bool states
  bool                        login_state;
  bool                        db_conn_state_;
  // enum
  userType                    user_type;
  // db controller
  DatabaseController*         db_controller_;


};

#endif // HOMESCREEN_H
